﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MCPU
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            routes.MapRoute(
                name: "AdminMCPURoutes",
                url: "Admin/MCPU/{controller}/{action}/{id}",
                defaults: new { controller = "Admin/Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "AdminSetupRoutes",
                url: "Admin/Setup/{controller}/{action}/{id}",
                defaults: new { controller = "Admin/Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "AdminSystemRoutes",
                url: "Admin/System/{controller}/{action}/{id}",
                defaults: new { controller = "Admin/Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "AdminRoutes",
                url: "Admin/{controller}/{action}/{id}",
                defaults: new { controller = "Admin/Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Login", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
