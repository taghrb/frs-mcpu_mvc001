﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace MCPU.Models
{
    public class YieldingModel
    {
        public static string GetNextProcessId(string rmtCode, DateTime batchDate)
        {
            string processId = "-";
            if (rmtCode != null && batchDate != null)
            {
                DateTime processDate;
                if (rmtCode.Trim().Length > 0 && DateTime.TryParse(batchDate.ToString(), out processDate))
                {
                    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
                    DataTable ProcessMaxNoTRX = dbHlpr.FetchData("SELECT TOP (1) PRH_BCHNO AS PRH_BCHNO "
                        + " FROM PRC_TRX_YLDHD "
                        + " WHERE PRH_RMCOD = '" + rmtCode + "' "
                        + " AND PRH_SDATE = '" + processDate + "' "
                        + " ORDER BY CONVERT(INT, PRH_BCHNO) DESC ");
                    DataTable ProcessMaxNoHDR = dbHlpr.FetchData("SELECT TOP (1) PRH_BCHNO AS PRH_BCHNO "
                        + " FROM PRC_HST_YLDHD "
                        + " WHERE PRH_RMCOD = '" + rmtCode + "' "
                        + " AND PRH_SDATE = '" + processDate + "' "
                        + " ORDER BY CONVERT(INT, PRH_BCHNO) DESC");

                    int MaxProcessNumber = 1;
                    if (ProcessMaxNoTRX.Rows.Count > 0 && ProcessMaxNoHDR.Rows.Count > 0)
                    {
                        if (Convert.ToInt32(ProcessMaxNoTRX.Rows[0]["PRH_BCHNO"].ToString()) > Convert.ToInt32(ProcessMaxNoHDR.Rows[0]["PRH_BCHNO"].ToString()))
                        {
                            MaxProcessNumber += Convert.ToInt32(ProcessMaxNoTRX.Rows[0]["PRH_BCHNO"].ToString());
                        }
                        else
                        {
                            MaxProcessNumber += Convert.ToInt32(ProcessMaxNoHDR.Rows[0]["PRH_BCHNO"].ToString());
                        }
                    }

                    if (ProcessMaxNoTRX.Rows.Count > 0 && ProcessMaxNoHDR.Rows.Count == 0)
                    {
                        MaxProcessNumber += Convert.ToInt32(ProcessMaxNoTRX.Rows[0]["PRH_BCHNO"].ToString());
                    }

                    if (ProcessMaxNoTRX.Rows.Count == 0 && ProcessMaxNoHDR.Rows.Count > 0)
                    {
                        MaxProcessNumber += Convert.ToInt32(ProcessMaxNoHDR.Rows[0]["PRH_BCHNO"].ToString());
                    }

                    string RmCode = rmtCode.Trim().Substring(rmtCode.Trim().IndexOf('-'));
                    RmCode = RmCode.Substring(RmCode.IndexOf('-') + 1);
                    processId = RmCode + DatabaseHelperClass.DateToForsanFormat(Convert.ToDateTime(batchDate)) + "-" + MaxProcessNumber;
                }
            }

            return processId;
        }

        public static DataTable GetProcessHeader(string rmCode, string processId)
        {
            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            DataTable YieldingDetails = dbHlpr.FetchData("SELECT "
                                                        + " PRH_ID, PRH_BCHNO, PRH_RMCOD, PRH_DESC1, PRH_SDATE, PRH_STIME, "
                                                        + " PRH_EDATE, PRH_ETIME, PRH_WEEK, PRH_WTKGS, PRH_REFNO, PRH_ENTBY, "
                                                        + " PRH_ENTDT, PRH_ENTIM, PRH_UPDBY, PRH_LSUPD, PRH_LSUPT "
                                                        + " FROM PRC_TRX_YLDHD "
                                                        + " WHERE PRH_RMCOD = '" + rmCode + "' AND PRH_ID = '" + processId + "'");
            return YieldingDetails;
        }
    }
}