﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;

namespace MCPU.Models
{
    public class MenuModel
    {
        [Required]
        public int Key { get; set; }
        [Required]
        public string Code { get; set; }
        [Required]
        public string Name { get; set; }

        public MenuModel()
        {
        }

        public MenuModel(int key, string code, string name)
        {
            this.Key = key;
            this.Code = code;
            this.Name = name;
        }

        internal static List<MenuModel> GetAll()
        {
            return GetWhere("");
        }

        internal static List<MenuModel> GetWhere(string where)
        {
            string qry = "SELECT * FROM STP_USR_MENUS";
            if (where.Length > 0)
            {
                qry += " WHERE " + where;
            }
            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            DataTable dt = dbHlpr.FetchData(qry);

            List<MenuModel> list = new List<MenuModel>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                list.Add(new MenuModel((int)dt.Rows[i]["MNU_KEY"], dt.Rows[i]["MNU_CODE"].ToString(), dt.Rows[i]["MNU_NAME"].ToString()));
            }

            return list;
        }

        internal static MenuModel GetByCode(string code)
        {
            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            DataTable myDT = dbHlpr.FetchData("SELECT * FROM STP_USR_MENUS WHERE MNU_CODE = '" + code + "'");
            MenuModel model = new MenuModel();
            if (myDT.Rows.Count > 0)
            {
                return new MenuModel((int)myDT.Rows[0]["MNU_KEY"], myDT.Rows[0]["MNU_CODE"].ToString(), myDT.Rows[0]["MNU_NAME"].ToString());
            }

            return null;
        }

        internal static List<MenuItemModel> GetPagesByCode(string code)
        {
            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            DataTable myDT = dbHlpr.FetchData(
                "SELECT * FROM STP_MNU_PAGES "
                + " JOIN STP_LNK_MNACS ON PAG_ID = MNA_PGID "
                + " WHERE MNA_MNCOD = '" + code + "'"
                );
            List<MenuItemModel> menuPages = new List<MenuItemModel>();
            for (int i = 0; i < myDT.Rows.Count; i++)
            {
                menuPages.Add(new MenuItemModel(
                    (int)myDT.Rows[i]["PAG_ID"],
                    myDT.Rows[i]["PAG_CODE"].ToString(),
                    myDT.Rows[i]["PAG_NAME"].ToString(),
                    myDT.Rows[i]["PAG_URL"].ToString(),
                    myDT.Rows[i]["PAG_TYPE"].ToString(),
                    (int)myDT.Rows[i]["PAG_PARNT"],
                    (int)myDT.Rows[i]["PAG_SEQNO"]
                    ));
            }

            return menuPages;
        }

        internal static bool DeleteMenu(string code)
        {
            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            dbHlpr.ExecuteNonQuery("DELETE FROM STP_LNK_MNACS WHERE MNA_MNCOD = '" + code + "'");
            dbHlpr.ExecuteNonQuery("DELETE FROM STP_USR_MENUS WHERE MNU_CODE = '" + code + "'");

            return true;
        }

        internal static void AddNewMenu(DataTable mnuData)
        {
            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            if (mnuData.Rows.Count > 0)
            {
                string menuCode = mnuData.Rows[0]["mnuCode"].ToString();
                dbHlpr.ExecuteNonQuery("DELETE FROM STP_LNK_MNACS WHERE MNA_MNCOD = '" + menuCode + "'");
                foreach (DataRow rw in mnuData.Rows)
                {
                    MenuItemModel pgDetails = MenuItemModel.GetById(rw["mnuPageId"].ToString());
                    
                    string pageCode = pgDetails.Code;
                    string pageId = pgDetails.Id.ToString();
                    string pageType = ((char)pgDetails.Type).ToString();
                    string pageName = pgDetails.Name;

                    dbHlpr.ExecuteNonQuery("INSERT INTO STP_LNK_MNACS ("
                        + " MNA_KEY, MNA_MNCOD, MNA_PGCOD, "
                        + " MNA_PGID, MNA_PGTYP, MNA_PGNAM "
                        + " ) VALUES ( "
                        + "'" + (menuCode + pageCode) + "', '" + menuCode + "', '" + pageCode + "', "
                        + "'" + pageId + "', '" + pageType + "', '" + pageName + "' )"
                        );
                }
            }
        }
    }
}