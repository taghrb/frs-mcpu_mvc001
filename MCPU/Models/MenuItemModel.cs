﻿using MCPU.Models.CustomValidations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;

namespace MCPU.Models
{
    public class MenuItemModel
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string Code { get; set; }

        [Required]
        public string Name { get; set; }

        [RequiredIf("Type", PageType.Page)]
        public string Url { get; set; }

        [Required]
        [EnumDataType(typeof(PageType))]
        public PageType Type { get; set; }

        // Has custom validation in Controller
        public int? Parent { get; set; }

        [Required]
        public int SeqNo { get; set; }

        public MenuItemModel()
        {
        }

        public MenuItemModel(int id, string code, string name, string url, string type, int parent, int seqNo)
        {
            if (type.Equals("T"))
            {
                Type = PageType.Top;
            }
            else if (type.Equals("G"))
            {
                Type = PageType.Group;
            }
            else if (type.Equals("P"))
            {
                Type = PageType.Page;
            }
            else if (type.Equals("R"))
            {
                Type = PageType.Role;
            }

            Id = id;
            Code = code;
            Name = name;
            Url = url;
            Parent = parent;
            SeqNo = seqNo;
        }

        public static List<EasyUiTreeNode> GetPagesTree(int parentId)
        {
            List<EasyUiTreeNode> toReturn = new List<EasyUiTreeNode>();

            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            DataTable pages = dbHlpr.FetchData("SELECT * FROM STP_MNU_PAGES WHERE PAG_PARNT = '" + parentId + "' ORDER BY PAG_SEQNO ASC");

            foreach (DataRow page in pages.Rows)
            {
                EasyUiTreeNode node = new EasyUiTreeNode();
                node.id = (int)page["PAG_ID"];
                node.text = page["PAG_CODE"] + " - " + page["PAG_NAME"];
                node.@checked = false;
                node.children = GetPagesTree((int)page["PAG_ID"]);

                toReturn.Add(node);
            }

            return toReturn;
        }

        internal static MenuItemModel GetByCode(string code)
        {
            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            DataTable myDT = dbHlpr.FetchData("SELECT * FROM STP_MNU_PAGES WHERE PAG_CODE = '" + code + "'");
            MenuItemModel model = new MenuItemModel();
            if (myDT.Rows.Count > 0)
            {
                model = new MenuItemModel(
                    (int)myDT.Rows[0]["PAG_ID"],
                    myDT.Rows[0]["PAG_CODE"].ToString(),
                    myDT.Rows[0]["PAG_NAME"].ToString(),
                    myDT.Rows[0]["PAG_URL"].ToString(),
                    myDT.Rows[0]["PAG_TYPE"].ToString(),
                    (int)myDT.Rows[0]["PAG_PARNT"],
                    (int)myDT.Rows[0]["PAG_SEQNO"]
                    );
            }

            return model;
        }

        internal static MenuItemModel GetById(string id)
        {
            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            DataTable myDT = dbHlpr.FetchData("SELECT * FROM STP_MNU_PAGES WHERE PAG_ID = '" + id + "'");
            MenuItemModel model = new MenuItemModel();
            if (myDT.Rows.Count > 0)
            {
                model = new MenuItemModel(
                    (int)myDT.Rows[0]["PAG_ID"],
                    myDT.Rows[0]["PAG_CODE"].ToString(),
                    myDT.Rows[0]["PAG_NAME"].ToString(),
                    myDT.Rows[0]["PAG_URL"].ToString(),
                    myDT.Rows[0]["PAG_TYPE"].ToString(),
                    (int)myDT.Rows[0]["PAG_PARNT"],
                    (int)myDT.Rows[0]["PAG_SEQNO"]
                    );
            }

            return model;
        }
    }

    public enum PageType
    {
        Top = 'T',
        Group = 'G',
        Page = 'P',
        Role = 'R'
    }

    public class EasyUiTreeNode
    {
        public int id;
        public string text;
        public bool @checked;
        public List<EasyUiTreeNode> children;
    }
}