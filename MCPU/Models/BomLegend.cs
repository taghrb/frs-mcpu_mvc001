﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace MCPU.Models
{
    public class BomLegend
    {
        public static DataTable GetByCode(string code)
        {
            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            DataTable dt = dbHlpr.FetchData("SELECT "
                + " BLG_CODE, BLG_NAME, BLG_UOM "
                + " FROM STP_MSTR_BOMLG "
                + " WHERE BLG_CODE = '" + code + "' ");

            return dt;
        }

        public static DataTable GetAll()
        {
            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            DataTable dt = dbHlpr.FetchData("SELECT "
                + " BLG_CODE, BLG_NAME, BLG_UOM "
                + " FROM STP_MSTR_BOMLG "
                + " ORDER BY BLG_CODE ");

            return dt;
        }
    }
}