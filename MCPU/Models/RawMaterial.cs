﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace MCPU.Models
{
    public class RawMaterial
    {
        public static DataTable GetByCode(string code)
        {
            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            DataTable dt = dbHlpr.FetchData("SELECT "
                    + " RMT_CODE, RMT_DESC1, RMT_DESC2, RMT_TRKMD, RMT_CATG, RMT_SCATG, "
                    + " RMT_UDF1, RMT_UDF2, RMT_UDF3, RMT_UDF4, RMT_UOM, RMT_WT, "
                    + " RMT_AVCST, RMT_ALCOD, RMT_STATS, RMT_LNKCD "
                    + " FROM INV_MSTR_RWMAT "
                    + " WHERE RMT_CODE = '" + code + "' ");

            return dt;
        }

        public static DataTable GetAll()
        {
            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            DataTable dt = dbHlpr.FetchData("SELECT "
                + " RMT_CODE, RMT_DESC1, RMT_DESC2, RMT_TRKMD, RMT_CATG, RMT_SCATG, "
                + " RMT_UDF1, RMT_UDF2, RMT_UDF3, RMT_UDF4, RMT_UOM, RMT_WT, "
                + " RMT_AVCST, RMT_ALCOD, RMT_STATS, RMT_LNKCD "
                + " FROM INV_MSTR_RWMAT "
                + " ORDER BY RMT_CODE ");

            return dt;
        }
    }
}