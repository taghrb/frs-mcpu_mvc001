﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;

namespace MCPU.Models
{
    public class DepartmentModel
    {
        [Required]
        public string Code { get; set; }
        [Required]
        public string Name { get; set; }

        public DepartmentModel()
        {
        }

        public DepartmentModel(string code, string name)
        {
            this.Code = code;
            this.Name = name;
        }

        internal static DepartmentModel GetByCode(string code)
        {
            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            DataTable myDT = dbHlpr.FetchData("SELECT * FROM STP_MSTR_DEPRT WHERE DPT_CODE = '" + code + "'");
            DepartmentModel model = new DepartmentModel();
            if (myDT.Rows.Count > 0)
            {
                return new DepartmentModel(myDT.Rows[0]["DPT_CODE"].ToString(), myDT.Rows[0]["DPT_NAME"].ToString());
            }

            return null;
        }

        internal static List<DepartmentModel> GetAll()
        {
            return GetWhere("");
        }

        internal static List<DepartmentModel> GetWhere(string where)
        {
            string qry = "SELECT * FROM STP_MSTR_DEPRT";
            if (where.Length > 0)
            {
                qry += " WHERE " + where;
            }
            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            DataTable dt = dbHlpr.FetchData(qry);

            List<DepartmentModel> list = new List<DepartmentModel>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                list.Add(new DepartmentModel(dt.Rows[i]["DPT_CODE"].ToString(), dt.Rows[i]["DPT_NAME"].ToString()));
            }

            return list;
        }
    }
}