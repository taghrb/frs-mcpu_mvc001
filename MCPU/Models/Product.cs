﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace MCPU.Models
{
    public class Product
    {
        public static DataTable GetByCode(string code)
        {
            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            DataTable dt = dbHlpr.FetchData("SELECT "
                + " PRO_CODE, PRO_DESC1, PRO_DESC2, "
                + " PRO_TRKMD, PRO_CATG, PRO_SCATG, "
                + " PRO_UDF1, PRO_UDF2, PRO_UDF3, PRO_UDF4, "
                + " PRO_UOM, PRO_WT, PRO_ALCOD, PRO_STATS, PRO_CKNG "
                + " FROM INV_MSTR_PRODT "
                + " WHERE PRO_CODE = '" + code + "' ");

            return dt;
        }

        public static DataTable GetAll()
        {
            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            DataTable dt = dbHlpr.FetchData("SELECT "
                + " PRO_CODE, PRO_DESC1, PRO_DESC2, "
                + " PRO_TRKMD, PRO_CATG, PRO_SCATG, "
                + " PRO_UDF1, PRO_UDF2, PRO_UDF3, PRO_UDF4, "
                + " PRO_UOM, PRO_WT, PRO_ALCOD, PRO_STATS, PRO_CKNG "
                + " FROM INV_MSTR_PRODT "
                + " ORDER BY PRO_CODE ");

            return dt;
        }

        internal static DataTable GetAllHavingProductionBom()
        {
            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            DataTable dt = dbHlpr.FetchData("SELECT "
                + " DISTINCT PRO_CODE, PRO_DESC1, PRO_DESC2, PRO_TRKMD, PRO_CATG, PRO_SCATG, "
                + " PRO_UDF1, PRO_UDF2, PRO_UDF3, PRO_UDF4, PRO_UOM, PRO_WT, "
                + " PRO_ALCOD, PRO_STATS "
                + " FROM INV_MSTR_PRODT "
                + " INNER JOIN BOM_MST_MRNTN ON PRO_CODE = MBM_PROCD "
                + " ORDER BY PRO_CODE ");

            return dt;
        }
    }
}