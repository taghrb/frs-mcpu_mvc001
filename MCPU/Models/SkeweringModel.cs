﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace MCPU.Models
{
    public class SkeweringModel
    {
        public static DataTable GetProcessHeader(string rmCode, string processId)
        {
            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            DataTable ProcessDetails = dbHlpr.FetchData("SELECT "
                                                        + " SPH_ID, SPH_BCHNO, SPH_PROCD, SPH_DESC1, SPH_SDATE, SPH_STIME, "
                                                        + " SPH_EDATE, SPH_ETIME, SPH_WEEK, SPH_WTKGS, SPH_REFNO, SPH_ENTBY, "
                                                        + " SPH_ENTDT, SPH_ENTIM, SPH_UPDBY, SPH_LSUPD, SPH_LSUPT "
                                                        + " FROM PRC_TRX_SKWHD "
                                                        + " WHERE SPH_PROCD = '" + rmCode + "' AND SPH_ID = '" + processId + "'");
            return ProcessDetails;
        }

        public static string GetNextProcessId(string prodCode, DateTime batchDate)
        {
            string processId = "";
            if (prodCode != null && batchDate != null)
            {
                DateTime processDate;
                if (prodCode.ToString().Trim().Length > 0 && DateTime.TryParse(batchDate.ToString(), out processDate))
                {
                    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
                    DataTable ProcessMaxNoTRX = dbHlpr.FetchData("SELECT TOP (1) SPH_BCHNO AS SPH_BCHNO "
                        + " FROM PRC_TRX_SKWHD "
                        + " WHERE SPH_PROCD = '" + prodCode.ToString() + "' "
                        + " AND SPH_SDATE = '" + processDate + "' "
                        + " ORDER BY CONVERT(INT, SPH_BCHNO) DESC ");
                    DataTable ProcessMaxNoHDR = dbHlpr.FetchData("SELECT TOP (1) SPH_BCHNO AS SPH_BCHNO "
                        + " FROM PRC_HST_SKWHD "
                        + " WHERE SPH_PROCD = '" + prodCode.ToString() + "' "
                        + " AND SPH_SDATE = '" + processDate + "' "
                        + " ORDER BY CONVERT(INT, SPH_BCHNO) DESC");

                    int MaxProcessNumber = 1;
                    if (ProcessMaxNoTRX.Rows.Count > 0 && ProcessMaxNoHDR.Rows.Count > 0)
                    {
                        if (Convert.ToInt32(ProcessMaxNoTRX.Rows[0]["SPH_BCHNO"].ToString()) > Convert.ToInt32(ProcessMaxNoHDR.Rows[0]["SPH_BCHNO"].ToString()))
                        {
                            MaxProcessNumber += Convert.ToInt32(ProcessMaxNoTRX.Rows[0]["SPH_BCHNO"].ToString());
                        }
                        else
                        {
                            MaxProcessNumber += Convert.ToInt32(ProcessMaxNoHDR.Rows[0]["SPH_BCHNO"].ToString());
                        }
                    }

                    if (ProcessMaxNoTRX.Rows.Count > 0 && ProcessMaxNoHDR.Rows.Count == 0)
                    {
                        MaxProcessNumber += Convert.ToInt32(ProcessMaxNoTRX.Rows[0]["SPH_BCHNO"].ToString());
                    }

                    if (ProcessMaxNoTRX.Rows.Count == 0 && ProcessMaxNoHDR.Rows.Count > 0)
                    {
                        MaxProcessNumber += Convert.ToInt32(ProcessMaxNoHDR.Rows[0]["SPH_BCHNO"].ToString());
                    }

                    string RmCode = prodCode.ToString().Trim().Substring(prodCode.ToString().Trim().IndexOf('-'));
                    RmCode = RmCode.Substring(RmCode.IndexOf('-') + 1);
                    processId = RmCode + DatabaseHelperClass.DateToForsanFormat(Convert.ToDateTime(batchDate)) + "-" + MaxProcessNumber;
                }
            }

            return processId;
        }
    }
}