﻿using MCPU.Models.CustomValidations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;

namespace MCPU.Models
{
    public class UserModel
    {
        [Required]
        public string UserId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string EmpId { get; set; }

        [Required]
        public string Password { get; set; }

        public string Designation { get; set; }

        [Required]
        public string Department { get; set; }

        [Required]
        public string Initials { get; set; }

        [Required]
        public string MenuCode { get; set; }

        public string Class { get; set; }

        public string Email { get; set; }

        public string Mobile { get; set; }

        public string Phone { get; set; }

        public string Extension { get; set; }

        public string Fax { get; set; }

        public string Company { get; set; }

        [Required]
        public int EditMasters { get; set; }

        public int? BackofficeAccess { get; set; }

        [Required]
        public int SystemAccess { get; set; }

        public UserModel()
        {
        }

        public UserModel(string userId, string name, string empId, string password, string designation, string department, string initials, string menuCode, string @class, string email, string mobile, string phone, string extension, string fax, string company, int editMasters, int backofficeAccess, int systemAccess)
        {
            UserId = userId;
            Name = name;
            EmpId = empId;
            Password = password;
            Designation = designation;
            Department = department;
            Initials = initials;
            MenuCode = menuCode;
            Class = @class;
            Email = email;
            Mobile = mobile;
            Phone = phone;
            Extension = extension;
            Fax = fax;
            Company = company;
            EditMasters = editMasters;
            BackofficeAccess = backofficeAccess;
            SystemAccess = systemAccess;
        }

        internal static UserModel GetByCode(string code)
        {
            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            DataTable myDT = dbHlpr.FetchData("SELECT * FROM STP_MSTR_USERS WHERE USR_ID = '" + code + "'");
            UserModel model = new UserModel();
            if (myDT.Rows.Count > 0)
            {
                int editMasters = 0;
                int boAccess = 0;
                int sysAccess = 0;
                try { editMasters = Convert.ToInt32(myDT.Rows[0]["USR_DTGOV"]); }
                catch { }

                try { boAccess = Convert.ToInt32(myDT.Rows[0]["USR_BOACS"]); }
                catch { }

                try { sysAccess = Convert.ToInt32(myDT.Rows[0]["USR_SYACS"]); }
                catch { }

                model = new UserModel(
                    myDT.Rows[0]["USR_ID"].ToString(),
                    myDT.Rows[0]["USR_NAME"].ToString(),
                    myDT.Rows[0]["USR_EMPID"].ToString(),
                    myDT.Rows[0]["USR_PSWRD"].ToString(),
                    myDT.Rows[0]["USR_DESG"].ToString(),
                    myDT.Rows[0]["USR_DEPT"].ToString(),
                    myDT.Rows[0]["USR_INITL"].ToString(),
                    myDT.Rows[0]["USR_MNUCD"].ToString(),
                    myDT.Rows[0]["USR_CLASS"].ToString(),
                    myDT.Rows[0]["USR_EMAIL"].ToString(),
                    myDT.Rows[0]["USR_MOBIL"].ToString(),
                    myDT.Rows[0]["USR_PHONE"].ToString(),
                    myDT.Rows[0]["USR_EXT"].ToString(),
                    myDT.Rows[0]["USR_FAX"].ToString(),
                    myDT.Rows[0]["USR_CMPNY"].ToString(),
                    editMasters, boAccess, sysAccess
                    );
                return model;
            }

            return null;
        }

        internal static List<UserModel> GetAll()
        {
            return GetAllWhere("");
        }

        internal static List<UserModel> GetAllWhere(string where)
        {
            string qry = "SELECT STP_MSTR_USERS.*, MNU_NAME, DPT_NAME "
                + " FROM STP_MSTR_USERS "
                + " LEFT JOIN STP_USR_MENUS ON USR_MNUCD = MNU_CODE "
                + " LEFT JOIN STP_MSTR_DEPRT ON USR_DEPT = DPT_CODE ";
            if (where.Length > 0)
            {
                qry += " WHERE " + where;
            }
            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            DataTable dt = dbHlpr.FetchData(qry);

            List<UserModel> usersList = new List<UserModel>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int editMasters = 0;
                int boAccess = 0;
                int sysAccess = 0;
                try { editMasters = Convert.ToInt32(dt.Rows[i]["USR_DTGOV"]); }
                catch { }

                try { boAccess = Convert.ToInt32(dt.Rows[i]["USR_BOACS"]); }
                catch { }

                try { sysAccess = Convert.ToInt32(dt.Rows[i]["USR_SYACS"]); }
                catch { }

                usersList.Add(new UserModel(
                    dt.Rows[i]["USR_ID"].ToString(),
                    dt.Rows[i]["USR_NAME"].ToString(),
                    dt.Rows[i]["USR_EMPID"].ToString(),
                    dt.Rows[i]["USR_PSWRD"].ToString(),
                    dt.Rows[i]["USR_DESG"].ToString(),
                    dt.Rows[i]["USR_DEPT"].ToString(),
                    dt.Rows[i]["USR_INITL"].ToString(),
                    dt.Rows[i]["USR_MNUCD"].ToString(),
                    dt.Rows[i]["USR_CLASS"].ToString(),
                    dt.Rows[i]["USR_EMAIL"].ToString(),
                    dt.Rows[i]["USR_MOBIL"].ToString(),
                    dt.Rows[i]["USR_PHONE"].ToString(),
                    dt.Rows[i]["USR_EXT"].ToString(),
                    dt.Rows[i]["USR_FAX"].ToString(),
                    dt.Rows[i]["USR_CMPNY"].ToString(),
                    editMasters, boAccess, sysAccess
                    ));
            }

            return usersList;
        }
    }
}