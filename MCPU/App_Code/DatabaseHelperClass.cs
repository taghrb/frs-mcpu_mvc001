﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;

namespace MCPU
{
    public class DatabaseHelperClass
    {
        private SqlConnection SQL_CONNECTION;

        public DatabaseHelperClass()
        {
            string connStr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            SQL_CONNECTION = new SqlConnection(connStr);
        }

        public object ExecuteScalarQuery(string qry)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(qry, SQL_CONNECTION);

                if (SQL_CONNECTION.State != ConnectionState.Open)
                {
                    SQL_CONNECTION.Open();
                }

                return cmd.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                SQL_CONNECTION.Close();
            }
        }

        public int ExecuteNonQuery(string qry)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(qry, SQL_CONNECTION);

                if (SQL_CONNECTION.State != ConnectionState.Open)
                {
                    SQL_CONNECTION.Open();
                }

                return cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                SQL_CONNECTION.Close();
            }
        }

        public DataTable FetchParameterizedData(string qry, params KeyValuePair<string, string>[] prms)
        {
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
            SqlCommand sqlCommand = new SqlCommand(qry);
            sqlCommand.Connection = SQL_CONNECTION;

            try
            {
                if (SQL_CONNECTION.State != ConnectionState.Open)
                {
                    SQL_CONNECTION.Open();
                }

                sqlCommand.CommandType = CommandType.StoredProcedure;
                foreach (KeyValuePair<string, string> param in prms)
                {
                    sqlCommand.Parameters.AddWithValue("@" + param.Key, param.Value);
                }

                sqlDataAdapter.SelectCommand = sqlCommand;
                DataTable sqlDataTable = new DataTable();
                sqlDataAdapter.Fill(sqlDataTable);

                return sqlDataTable;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                SQL_CONNECTION.Close();
            }
        }

        public void ExecuteParameterizedQuery(string qry, params KeyValuePair<string, string>[] prms)
        {
            SqlCommand sqlCommand = new SqlCommand(qry);

            try
            {
                if (SQL_CONNECTION.State != ConnectionState.Open)
                {
                    SQL_CONNECTION.Open();
                }

                sqlCommand.Connection = SQL_CONNECTION;
                sqlCommand.CommandType = CommandType.StoredProcedure;

                foreach (KeyValuePair<string, string> param in prms)
                {
                    sqlCommand.Parameters.AddWithValue("@" + param.Key, param.Value);
                }

                sqlCommand.ExecuteNonQuery();
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                SQL_CONNECTION.Close();
            }
        }

        public DataTable FetchData(string qry)
        {
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(qry, SQL_CONNECTION);

            try
            {
                SQL_CONNECTION.Open();

                DataTable sqlDataTable = new DataTable();
                sqlDataAdapter.Fill(sqlDataTable);

                return sqlDataTable;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                SQL_CONNECTION.Close();
            }
        }


        public void InsertUserLog(string userId, string userName, DateTime dateTimeNow, string comment, string logExtra)
        {
            try
            {
                string date = dateTimeNow.ToString("yyyy-MM-dd");
                string time = dateTimeNow.ToString("HH:mm:ss");

                string qry = string.Format("INSERT INTO SYS_MSTR_ULOGS (LOG_USRID, LOG_USRNM, LOG_DATE, LOG_TIME, LOG_COMNT, LOG_EXTRA "
                    + " ) VALUES ( @userId, @userName, @date, @time, @comment, @extra );");

                if (SQL_CONNECTION.State != ConnectionState.Open)
                {
                    SQL_CONNECTION.Open();
                }

                SqlCommand sqlCommand = new SqlCommand(qry);
                sqlCommand.Connection = SQL_CONNECTION;
                sqlCommand.CommandType = CommandType.Text;

                sqlCommand.Parameters.AddWithValue("@userId", userId == null ? "*NULL" : userId);
                sqlCommand.Parameters.AddWithValue("@userName", userName == null ? "*NULL" : userName);
                sqlCommand.Parameters.AddWithValue("@date", date);
                sqlCommand.Parameters.AddWithValue("@time", time);
                sqlCommand.Parameters.AddWithValue("@comment", "WinApp: " + comment);
                sqlCommand.Parameters.AddWithValue("@extra", logExtra);

                sqlCommand.ExecuteNonQuery();
            }
            catch (Exception eex)
            {
                throw eex;
            }
        }

        public String GetRptSeqNo()
        {
            int rptno = 0;
            int rpt_len = 0;
            string NewRPTNo = string.Empty;
            try
            {
                String NewQuery = "SELECT RPTNO FROM SYS_RPT_SEQNO ";
                DataTable dt = new DataTable();
                SqlDataAdapter DAdp = new SqlDataAdapter(NewQuery, SQL_CONNECTION);

                if (SQL_CONNECTION.State != ConnectionState.Open)
                {
                    SQL_CONNECTION.Open();
                }

                DAdp.Fill(dt);
                if (dt.Rows[0][0] != DBNull.Value)
                {
                    rptno = Convert.ToInt32(dt.Rows[0][0]);
                }
                else
                {
                    NewRPTNo = "00001";
                }
                NewRPTNo = Convert.ToString(rptno + 1);
                rpt_len = NewRPTNo.Length;

                if (rpt_len == 1)
                { NewRPTNo = "0000" + NewRPTNo; }
                else if (rpt_len == 2)
                { NewRPTNo = "000" + NewRPTNo; }
                else if (rpt_len == 3)
                { NewRPTNo = "00" + NewRPTNo; }
                else if (rpt_len == 4)
                { NewRPTNo = "0" + NewRPTNo; }


                string UpdateRPTNo = "UPDATE SYS_RPT_SEQNO SET RPTNO ='" + NewRPTNo + "'";
                ExecuteNonQuery(UpdateRPTNo);

                return NewRPTNo;
            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                SQL_CONNECTION.Close();
            }
        }

        /*** Uncommon Functions, to handle DB Actions ***/

        public static string DateToForsanFormat(DateTime date)
        {
            try
            {
                string dateToReturn = "";

                dateToReturn += date.Day.ToString().Length == 1 ? "0" + date.Day.ToString() : date.Day.ToString();
                switch (date.Month)
                {
                    case 1:
                        dateToReturn += "A";
                        break;
                    case 2:
                        dateToReturn += "B";
                        break;
                    case 3:
                        dateToReturn += "C";
                        break;
                    case 4:
                        dateToReturn += "D";
                        break;
                    case 5:
                        dateToReturn += "E";
                        break;
                    case 6:
                        dateToReturn += "F";
                        break;
                    case 7:
                        dateToReturn += "G";
                        break;
                    case 8:
                        dateToReturn += "H";
                        break;
                    case 9:
                        dateToReturn += "I";
                        break;
                    case 10:
                        dateToReturn += "J";
                        break;
                    case 11:
                        dateToReturn += "K";
                        break;
                    case 12:
                        dateToReturn += "L";
                        break;
                    default:
                        break;
                }
                dateToReturn += date.Year.ToString().Substring(2, 2);

                return dateToReturn;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public static string ConvertToArabic(string input)
        {
            System.Text.UTF8Encoding utf8Encoder = new UTF8Encoding();
            System.Text.Decoder utf8Decoder = utf8Encoder.GetDecoder();
            System.Text.StringBuilder convertedChars = new System.Text.StringBuilder();
            char[] convertedChar = new char[1];
            byte[] bytes = new byte[] { 217, 160 };
            char[] inputCharArray = input.ToCharArray();
            foreach (char c in inputCharArray)
            {
                if (char.IsDigit(c))
                {
                    bytes[1] = Convert.ToByte(160 + char.GetNumericValue(c));
                    utf8Decoder.GetChars(bytes, 0, 2, convertedChar, 0);
                    convertedChars.Append(convertedChar[0]);
                }
                else
                {
                    if (c == 44)
                    {
                        convertedChars.Append(Convert.ToChar(184));
                    }
                    else if (c == 46)
                    {
                        convertedChars.Append(Convert.ToChar(44));
                    }
                    else
                    {
                        convertedChars.Append(c);
                    }
                }
            }
            string strr = convertedChars.ToString();
            return convertedChars.ToString();
        }
    }
}