﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MCPU.Controllers
{
    public class LoginController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Login";

            return View("Index");
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View("Index");
        }

        [HttpPost]
        public ActionResult Login(FormCollection form)
        {
            string strEmail = form["username"];
            string strPassword = form["password"];

            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            DataTable dtUsr = dbHlpr.FetchData("SELECT * FROM STP_MSTR_USERS WHERE USR_ID = '" + strEmail + "' AND USR_PSWRD = '" + strPassword + "';");

            // Validate the user password
            if (dtUsr.Rows.Count > 0)
            {
                Session["isLoggedIn"] = true;
                //  Session["ADC"] = strActiveDivision + "_"; // Active division code, will be used throughout the system

                Session["userObject"] = dtUsr;
                Session["UserId"] = dtUsr.Rows[0]["USR_ID"].ToString();
                Session["UserName"] = dtUsr.Rows[0]["USR_NAME"].ToString();
                Session["userMenuCode"] = dtUsr.Rows[0]["USR_MNUCD"].ToString();

                //DataTable dtMenuInfo = dbHlpr.FetchData("SELECT * FROM STP_LNK_MNACS JOIN STP_MNU_PAGES ON MPG_PGID = PAG_ID");
                DataTable dtMenuInfo = dbHlpr.FetchData("SELECT * FROM STP_LNK_MNACS JOIN STP_MNU_PAGES ON MNA_PGCOD = PAG_CODE");
                Session["userMenuDataTable"] = dtMenuInfo;

                return Redirect("~/Admin/Home/");
            }
            else
            {
                ViewBag.ErrMsg = "Invalid username or password.";

                return Index();
            }
        }


        [HttpGet]
        public ActionResult Logout()
        {
            Session.Remove("isLoggedIn");
            Session.Remove("UserId");
            Session.Remove("userObject");
            Session.Remove("userMenuCode");
            Session.Remove("userMenuDataTable");
            Session.Remove("userBranchCode");
            Session.Remove("ADC");
            Session.Contents.Clear();

            return Redirect("~/Login/");
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string actionName = filterContext.ActionDescriptor.ActionName;
            if (Session["isLoggedIn"] != null && (bool)Session["isLoggedIn"] == true && actionName != "Logout")
            {
                filterContext.Result = Redirect("~/Admin/Home/");
            }
            else
            {
                if (actionName == "Index" || actionName == "Login" || actionName == "Logout")
                {
                    base.OnActionExecuting(filterContext);
                }
                else
                {
                    filterContext.Result = new RedirectResult("~/Login/");
                }
            }
        }
    }
}