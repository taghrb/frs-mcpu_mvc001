﻿using MCPU.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MCPU.Controllers.Admin.MCPU
{
    public class ProductionController : AuthController
    {
        string BOM_VIEW = "~/Views/Admin/MCPU/Production/BoM.cshtml";
        string PROCESS_VIEW = "~/Views/Admin/MCPU/Production/Process.cshtml";
        string PROCESSINQUIRY_VIEW = "~/Views/Admin/MCPU/Production/ProcessInquiry.cshtml";
        public ActionResult BoM()
        {
            DataTable dt = Product.GetAll();

            ViewBag.PRDCTS = new SelectList(dt.AsDataView(), "PRO_CODE", "PRO_CODE");

            return View(BOM_VIEW);
        }

        [HttpPost]
        public ActionResult LoadItems(string catg)
        {
            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            string qry = "SELECT "
                + " RMT_CODE, RMT_DESC1, RMT_DESC2, RMT_TRKMD, RMT_CATG, RMT_SCATG, "
                + " RMT_UDF1, RMT_UDF2, RMT_UDF3, RMT_UDF4, RMT_UOM, RMT_WT, "
                + " RMT_AVCST, RMT_ALCOD, RMT_STATS, RMT_LNKCD "
                + " FROM INV_MSTR_RWMAT "
                + " ORDER BY RMT_CODE ";
            if (catg == "MISC")
            {
                qry = " SELECT "
                    + " BLG_CODE AS RMT_CODE, BLG_NAME AS RMT_DESC1, BLG_UOM AS RMT_UOM, "
                    + " '' AS RMT_DESC2, '' AS RMT_TRKMD, '' AS RMT_CATG, '' AS RMT_SCATG, "
                    + " '' AS RMT_UDF1, '' AS RMT_UDF2, '' AS RMT_UDF3, '' AS RMT_UDF4, '1' AS RMT_WT, "
                    + " '0.00' AS RMT_AVCST, '' AS RMT_ALCOD, '' AS RMT_STATS, '' AS RMT_LNKCD "
                    + " FROM STP_MSTR_BOMLG ";
            }
            DataTable dt = dbHlpr.FetchData(qry);

            return Json(new { success = true, data = JsonConvert.SerializeObject(dt) });
        }

        [HttpPost]
        public ActionResult GetBomByProductCode(string code)
        {
            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            DataTable YldBoMHistDetails = dbHlpr.FetchData("SELECT "
                        + " MBM_KEY, MBM_PROCD, MBM_DESC1, "
                        + " MBM_ITMNO, MBM_ITFLG, MBM_SEQNO, "
                        + " MBM_ENTBY, MBM_ENTDT, MBM_ENTIM, "
                        + " MBM_UPDBY, MBM_LSUPD, MBM_LSUPT, "
                        + " MBM_PSTBY, MBM_PSTDT, MBM_PSTIM "
                        + " FROM BOM_MST_MRNTN "
                        + " WHERE MBM_PROCD = '" + code + "' ");
            if (YldBoMHistDetails.Rows.Count > 0)
            {
                return Json(new
                {
                    success = false,
                    msg = "Attention: BoM already exist and posted."
                });
            }

            DataTable DT_USABLES = new DataTable();
            DataTable DT_INGREDIENTS = new DataTable();
            DataTable DT_MISC = new DataTable();
            DataTable YldBoMTrxDetails = dbHlpr.FetchData("SELECT "
                + " MBM_KEY, MBM_PROCD, MBM_DESC1, "
                + " MBM_ITMNO, MBM_ITFLG, MBM_SEQNO, "
                + " MBM_ENTBY, MBM_ENTDT, MBM_ENTIM, "
                + " MBM_UPDBY, MBM_LSUPD, MBM_LSUPT "
                + " FROM BOM_TRX_MRNTN "
                + " WHERE MBM_PROCD = '" + code + "' ");
            if (YldBoMTrxDetails.Rows.Count > 0)
            {
                DT_USABLES = dbHlpr.FetchData("SELECT "
                            + " MBM_ITMNO AS ITMCOD, RMT_DESC1 AS ITMDESC1, RMT_UOM AS ITMUOM, MBM_FRMLA AS ITMFRMLA "
                            + " FROM BOM_TRX_MRNTN "
                            + " LEFT JOIN INV_MSTR_RWMAT ON MBM_ITMNO = RMT_CODE "
                            + " WHERE MBM_ITFLG = '2' "
                            + " AND MBM_PROCD = '" + code + "' "
                            + " ORDER BY MBM_SEQNO ");
                DT_INGREDIENTS = dbHlpr.FetchData("SELECT "
                            + " MBM_ITMNO AS ITMCOD, RMT_DESC1 AS ITMDESC1, RMT_UOM AS ITMUOM, MBM_FRMLA AS ITMFRMLA "
                            + " FROM BOM_TRX_MRNTN "
                            + " LEFT JOIN INV_MSTR_RWMAT ON MBM_ITMNO = RMT_CODE "
                            + " WHERE MBM_ITFLG = '3' "
                            + " AND MBM_PROCD = '" + code + "' "
                            + " ORDER BY MBM_SEQNO ");
                DT_MISC = dbHlpr.FetchData("SELECT "
                    + " BLG_CODE AS ITMCOD, BLG_NAME AS ITMDESC1, BLG_UOM AS ITMUOM, MBM_FRMLA AS ITMFRMLA "
                    + " FROM BOM_TRX_MRNTN "
                    + " LEFT JOIN STP_MSTR_BOMLG ON MBM_ITMNO = BLG_CODE "
                    + " WHERE MBM_ITFLG = '4' "
                    + " AND MBM_PROCD = '" + code + "' "
                    + " ORDER BY MBM_SEQNO ");
            }

            return Json(new
            {
                success = true,
                umtData = JsonConvert.SerializeObject(DT_USABLES),
                ingData = JsonConvert.SerializeObject(DT_INGREDIENTS),
                mscData = JsonConvert.SerializeObject(DT_MISC)
            });
        }

        [HttpPost]
        public ActionResult SaveBoM(FormCollection form)
        {
            if (form["prod_cd"] == null || form["prod_cd"].ToString().Trim().Length <= 0)
            {
                return Json(new
                {
                    success = false,
                    msg = "Error :: Please select a Processed item to save."
                });
            }

            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            DateTime dtNow = DateTime.Now;
            string ItmCode = form["prod_cd"].ToString().Trim();
            dbHlpr.ExecuteNonQuery("DELETE FROM BOM_TRX_MRNTN WHERE MBM_PROCD = '" + ItmCode + "' ");
            string[] DT_USABLES = form["umt_items[]"] == null ? new string[] { } : form["umt_items[]"].Split(',');
            string[] DT_UMFRMLA = form["umt_frmla[]"] == null ? new string[] { } : form["umt_frmla[]"].Split(',');
            string[] DT_INGREDIENTS = form["ing_items[]"] == null ? new string[] { } : form["ing_items[]"].Split(',');
            string[] DT_INGFRMLA = form["ing_frmla[]"] == null ? new string[] { } : form["ing_frmla[]"].Split(',');
            string[] DT_MISC = form["msc_items[]"] == null ? new string[] { } : form["msc_items[]"].Split(',');
            string[] DT_MSCFRMLA = form["msc_frmla[]"] == null ? new string[] { } : form["msc_frmla[]"].Split(',');
            if (DT_USABLES.Length > 0 || DT_INGREDIENTS.Length > 0 || DT_MISC.Length > 0)
            {
                DataTable dt = Product.GetByCode(ItmCode);
                string prodDesc1 = dt.Rows[0]["PRO_DESC1"].ToString();
                // Insert Rows for Meat
                for (int i = 0; i < DT_USABLES.Length; i++)
                {
                    string itemNo = DT_USABLES[i];
                    string ItemTypeFlag = "2";
                    int SeqNo = i + 1;
                    float frmla = 0;
                    try { frmla = float.Parse(DT_UMFRMLA[i].ToString()); }
                    catch { }

                    dbHlpr.ExecuteNonQuery("INSERT INTO BOM_TRX_MRNTN ( "
                        + " MBM_KEY, MBM_PROCD, MBM_DESC1, "
                        + " MBM_ITMNO, MBM_ITFLG, MBM_SEQNO, MBM_FRMLA, "
                        + " MBM_ENTBY, MBM_ENTDT, MBM_ENTIM, "
                        + " MBM_UPDBY, MBM_LSUPD, MBM_LSUPT "
                        + " ) VALUES ( "
                        + " '" + ItmCode + itemNo + "', '" + ItmCode + "', '" + prodDesc1 + "', "
                        + " '" + itemNo + "', '" + ItemTypeFlag + "', '" + SeqNo + "', '" + frmla + "', "
                        + " '" + Session["UserId"] + "', '" + dtNow + "', '" + dtNow + "', "
                        + " '" + Session["UserId"] + "', '" + dtNow + "', '" + dtNow + "' "
                        + " ) ");
                }

                // Insert Rows for Ingredients
                for (int i = 0; i < DT_INGREDIENTS.Length; i++)
                {
                    string itemNo = DT_INGREDIENTS[i].ToString();
                    string ItemTypeFlag = "3";
                    int SeqNo = DT_USABLES.Length + i + 1;
                    float frmla = 0;
                    try { frmla = float.Parse(DT_INGFRMLA[i].ToString()); }
                    catch { }

                    dbHlpr.ExecuteNonQuery("INSERT INTO BOM_TRX_MRNTN ( "
                        + " MBM_KEY, MBM_PROCD, MBM_DESC1, "
                        + " MBM_ITMNO, MBM_ITFLG, MBM_SEQNO, MBM_FRMLA, "
                        + " MBM_ENTBY, MBM_ENTDT, MBM_ENTIM, "
                        + " MBM_UPDBY, MBM_LSUPD, MBM_LSUPT "
                        + " ) VALUES ( "
                        + " '" + ItmCode + itemNo + "', '" + ItmCode + "', '" + prodDesc1 + "', "
                        + " '" + itemNo + "', '" + ItemTypeFlag + "', '" + SeqNo + "', '" + frmla + "', "
                        + " '" + Session["UserId"] + "', '" + dtNow + "', '" + dtNow + "', "
                        + " '" + Session["UserId"] + "', '" + dtNow + "', '" + dtNow + "' "
                        + " ) ");
                }

                // Insert Rows for Misc.
                for (int i = 0; i < DT_MISC.Length; i++)
                {
                    string itemNo = DT_MISC[i].ToString();
                    string ItemTypeFlag = "4";
                    int SeqNo = DT_USABLES.Length + DT_INGREDIENTS.Length + i + 1;
                    float frmla = 0;
                    try { frmla = float.Parse(DT_MSCFRMLA[i].ToString()); }
                    catch { }

                    dbHlpr.ExecuteNonQuery("INSERT INTO BOM_TRX_MRNTN ( "
                        + " MBM_KEY, MBM_PROCD, MBM_DESC1, "
                        + " MBM_ITMNO, MBM_ITFLG, MBM_SEQNO, MBM_FRMLA, "
                        + " MBM_ENTBY, MBM_ENTDT, MBM_ENTIM, "
                        + " MBM_UPDBY, MBM_LSUPD, MBM_LSUPT "
                        + " ) VALUES ( "
                        + " '" + ItmCode + itemNo + "', '" + ItmCode + "', '" + prodDesc1 + "', "
                        + " '" + itemNo + "', '" + ItemTypeFlag + "', '" + SeqNo + "', '" + frmla + "', "
                        + " '" + Session["UserId"] + "', '" + dtNow + "', '" + dtNow + "', "
                        + " '" + Session["UserId"] + "', '" + dtNow + "', '" + dtNow + "' "
                        + " ) ");
                }

                SetSuccessMessage("BoM Saved Successfully");

                return Json(new { success = true, url = Url.Action("BoM") });
            }
            else
            {
                return Json(new
                {
                    success = false,
                    msg = "Error :: No items added, unable to save."
                });
            }
        }

        [HttpPost]
        public ActionResult PostBoM(FormCollection form)
        {
            if (form["prod_cd"] == null || form["prod_cd"].ToString().Trim().Equals(""))
            {
                return Json(new
                {
                    success = false,
                    msg = "Error :: No BoM loaded to Post"
                });
            }

            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            DateTime dateTimeNow = DateTime.Now;
            string dateTimeNowDate = dateTimeNow.ToString("yyyy-MM-dd");
            string dateTimeNowTime = dateTimeNow.ToString("HH:mm:ss.fff");
            string ProdCd = form["prod_cd"].ToString().Trim();

            dbHlpr.ExecuteScalarQuery("INSERT INTO BOM_MST_MRNTN ( "
                + " MBM_KEY, MBM_PROCD, MBM_DESC1, "
                + " MBM_ITMNO, MBM_ITFLG, MBM_SEQNO, MBM_FRMLA, "
                + " MBM_ENTBY, MBM_ENTDT, MBM_ENTIM, "
                + " MBM_UPDBY, MBM_LSUPD, MBM_LSUPT, "
                + " MBM_PSTBY, MBM_PSTDT, MBM_PSTIM "
                + " ) SELECT "
                + " MBM_KEY, MBM_PROCD, MBM_DESC1, "
                + " MBM_ITMNO, MBM_ITFLG, MBM_SEQNO, MBM_FRMLA, "
                + " MBM_ENTBY, MBM_ENTDT, MBM_ENTIM, "
                + " MBM_UPDBY, MBM_LSUPD, MBM_LSUPT, "
                + " '" + Session["UserId"] + "', '" + dateTimeNowDate + "', '" + dateTimeNowTime + "' "
                + " FROM BOM_TRX_MRNTN "
                + " WHERE BOM_TRX_MRNTN.MBM_PROCD = '" + ProdCd + "'");

            dbHlpr.ExecuteScalarQuery("DELETE FROM BOM_TRX_MRNTN WHERE MBM_PROCD = '" + ProdCd + "'");

            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            string comments = "Posted MCPU Marination/Production BoM for "
                + ProdCd + " into "
                + controllerName + "/" + actionName + " form at " + System.DateTime.Now.ToString();
            string extras = "YBM of RM Code " + ProdCd + " Posted";
            dbHlpr.InsertUserLog(Session["UserId"].ToString(), Session["UserName"].ToString(), DateTime.Now, comments, extras);

            SetSuccessMessage("BoM Posted Successfully.");

            return Json(new { success = true, url = Url.Action("BoM") });
        }

        public ActionResult ProcessInquiry()
        {
            DataTable dt = Product.GetAll();

            ViewBag.PRDCTS = new SelectList(dt.AsDataView(), "PRO_CODE", "PRO_CODE");

            return View(PROCESSINQUIRY_VIEW);
        }

        [HttpPost]
        public ActionResult GetOpenProcessesByProd(string code)
        {
            DataTable dt = Product.GetByCode(code);

            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            DataTable dtData = dbHlpr.FetchData("SELECT "
                        + " MPH_ID, MPH_BCHNO, MPH_PROCD, MPH_DESC1, "
                        + " MPH_SDATE, MPH_STIME, MPH_EDATE, MPH_ETIME, "
                        + " MPH_WEEK, MPH_WTKGS, MPH_ENTBY, MPH_ENTDT, MPH_ENTIM, "
                        + " MPH_UPDBY, MPH_LSUPD, MPH_LSUPT "
                        + " FROM PRC_TRX_MRNHD "
                        + " WHERE MPH_PROCD = '" + code + "' ");

            return Json(new { success = true, data = JsonConvert.SerializeObject(dt), processes = JsonConvert.SerializeObject(dtData) });
        }

        public ActionResult Process(string prod, string prcs)
        {
            DataTable dtProd = Product.GetAll();
            DataTable dtRmt = RawMaterial.GetAll();
            ViewBag.PRDCTS = new SelectList(dtProd.AsDataView(), "PRO_CODE", "PRO_CODE");
            ViewBag.RMTS = new SelectList(dtRmt.AsDataView(), "RMT_CODE", "RMT_CODE");

            ViewBag.BatchStartDateEnabled = true;
            ViewBag.BatchStartDate = "";
            ViewBag.BatchEndDate = "";
            ViewBag.BatchStartTime = "";
            ViewBag.BatchEndTime = "";
            ViewBag.ProcessId = "";
            ViewBag.ProdCodeReadOnly = false;
            ViewBag.RefNo = "";
            ViewBag.PostEnabled = false;
            if (prod != null && prcs != null && prod != "" && prcs != "")
            {

                DataTable ProductionDetails = ProductionModel.GetProcessHeader(prod, prcs);
                if (ProductionDetails.Rows.Count > 0)
                {
                    ViewBag.PRDCTS = new SelectList(dtProd.AsDataView(), "PRO_CODE", "PRO_CODE", prod);
                    ViewBag.ProdCode = prod;
                    ViewBag.ProcessId = prcs;
                    ViewBag.PostEnabled = true;
                    ViewBag.BatchStartDateEnabled = false;

                    try
                    {
                        ViewBag.BatchStartDate = Convert.ToDateTime(ProductionDetails.Rows[0]["MPH_SDATE"].ToString()).ToString("dd-MMM-yyyy");
                    }
                    catch (Exception ex)
                    {
                        ViewBag.BatchStartDate = "";
                    }

                    try
                    {
                        ViewBag.BatchEndDate = Convert.ToDateTime(ProductionDetails.Rows[0]["MPH_EDATE"].ToString()).ToString("dd-MMM-yyyy");
                    }
                    catch (Exception ex)
                    {
                        ViewBag.BatchEndDate = "";
                    }

                    try
                    {
                        ViewBag.BatchStartTime = Convert.ToDateTime(ProductionDetails.Rows[0]["MPH_STIME"].ToString()).ToString("HH:mm");
                    }
                    catch (Exception ex)
                    {
                        ViewBag.BatchStartTime = "00:00";
                    }

                    try
                    {
                        ViewBag.BatchEndTime = Convert.ToDateTime(ProductionDetails.Rows[0]["MPH_ETIME"].ToString()).ToString("HH:mm");
                    }
                    catch (Exception ex)
                    {
                        ViewBag.BatchEndTime = "00:00";
                    }

                    ViewBag.ProdCodeReadOnly = true;
                    ViewBag.RefNo = ProductionDetails.Rows[0]["MPH_REFNO"].ToString();
                }
            }

            return View(PROCESS_VIEW);
        }

        [HttpPost]
        public ActionResult LoadProcess(string code, string prcs, string sndr, bool prodRdOnly)
        {
            if (sndr.ToLower() == "prod")
            {
                DataTable DT_MEAT = new DataTable();
                DataTable DT_INGRDNTS = new DataTable();
                DataTable DT_MISC = new DataTable();

                string desc1 = "";
                string desc2 = "";
                string uom = "";

                string processId = "";
                try
                {
                    processId = ProductionModel.GetNextProcessId(code, Convert.ToDateTime(prcs));
                }
                catch (Exception)
                {
                    processId = prcs;
                }

                if (code != null)
                {
                    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
                    DataTable dt = dbHlpr.FetchData("SELECT "
                        + " PRO_CODE, PRO_DESC1, PRO_DESC2, PRO_TRKMD, PRO_CATG, PRO_SCATG, "
                        + " PRO_UDF1, PRO_UDF2, PRO_UDF3, PRO_UDF4, PRO_UOM, PRO_WT, "
                        + " PRO_ALCOD, PRO_STATS "
                        + " FROM INV_MSTR_PRODT "
                        + " WHERE PRO_CODE = '" + code + "' ");
                    if (dt.Rows.Count > 0)
                    {
                        desc1 = dt.Rows[0]["PRO_DESC1"].ToString();
                        desc2 = dt.Rows[0]["PRO_DESC2"].ToString();
                        uom = dt.Rows[0]["PRO_UOM"].ToString();

                        if (prodRdOnly)
                        {
                            DT_MEAT = dbHlpr.FetchData("SELECT "
                            + " MPL_PRCID AS ITMPROCSID, MPL_GRVNO AS ITMGRV, MPL_ITMCD AS ITMCOD, RMT_DESC1 AS ITMDESC1, COALESCE(MPL_QTY, 0.000) AS ITMQTY "
                            + " FROM PRC_TRX_MRNLN "
                            + " LEFT JOIN BOM_MST_MRNTN ON MPL_ITMCD = MBM_ITMNO AND MPL_PROCD = MBM_PROCD "
                            + " LEFT JOIN INV_MSTR_RWMAT ON MPL_ITMCD = RMT_CODE "
                            + " WHERE MPL_TYPE = 'UMT' "
                            + " AND MPL_PROCD = '" + code + "' AND MPL_ID = '" + prcs + "' "
                            + " ORDER BY MBM_SEQNO ");
                            DT_INGRDNTS = dbHlpr.FetchData("SELECT "
                                + " MPL_GRVNO AS ITMGRV, MPL_ITMCD AS ITMCOD, RMT_DESC1 AS ITMDESC1, COALESCE(MPL_QTY, 0.000) AS ITMQTY, RMT_UOM AS ITMUOM "
                                + " FROM PRC_TRX_MRNLN "
                                + " LEFT JOIN BOM_MST_MRNTN ON MPL_ITMCD = MBM_ITMNO AND MPL_PROCD = MBM_PROCD "
                                + " LEFT JOIN INV_MSTR_RWMAT ON MPL_ITMCD = RMT_CODE "
                                + " WHERE MPL_TYPE = 'ING' "
                                + " AND MPL_PROCD = '" + code + "' AND MPL_ID = '" + prcs + "' "
                                + " ORDER BY MBM_SEQNO ");
                            DT_MISC = dbHlpr.FetchData("SELECT "
                                + " MPL_ITMCD AS ITMCOD, BLG_NAME AS ITMDESC1, COALESCE(MPL_QTY, 0.000) AS ITMQTY "
                                + " FROM PRC_TRX_MRNLN "
                                + " LEFT JOIN BOM_MST_MRNTN ON MPL_ITMCD = MBM_ITMNO AND MPL_PROCD = MBM_PROCD "
                                + " LEFT JOIN STP_MSTR_BOMLG ON MPL_ITMCD = BLG_CODE "
                                + " WHERE MPL_TYPE = 'MSC' "
                                + " AND MPL_PROCD = '" + code + "' AND MPL_ID = '" + prcs + "' "
                                + " ORDER BY MBM_SEQNO ");
                        }
                        else
                        {
                            DT_MEAT = dbHlpr.FetchData("SELECT "
                            + " '' AS ITMPROCSID, '' AS ITMGRV, MBM_ITMNO AS ITMCOD, RMT_DESC1 AS ITMDESC1, COALESCE(MPL_QTY, 0.000) AS ITMQTY "
                            + " FROM BOM_MST_MRNTN "
                            + " LEFT JOIN INV_MSTR_RWMAT ON MBM_ITMNO = RMT_CODE "
                            + " LEFT JOIN PRC_TRX_MRNLN ON MBM_ITMNO = MPL_ITMCD AND MPL_TYPE = 'UMT' AND MPL_ID = '" + prcs + "' "
                            + " WHERE MBM_ITFLG = '2' "
                            + " AND MBM_PROCD = '" + code + "' "
                            + " ORDER BY MBM_SEQNO ");
                            DT_INGRDNTS = dbHlpr.FetchData("SELECT "
                                + " '' AS ITMGRV, MBM_ITMNO AS ITMCOD, RMT_DESC1 AS ITMDESC1, COALESCE(MPL_QTY, 0.000) AS ITMQTY, RMT_UOM AS ITMUOM "
                                + " FROM BOM_MST_MRNTN "
                                + " LEFT JOIN INV_MSTR_RWMAT ON MBM_ITMNO = RMT_CODE "
                                + " LEFT JOIN PRC_TRX_MRNLN ON MBM_ITMNO = MPL_ITMCD AND MPL_TYPE = 'WST' AND MPL_ID = '" + prcs + "' "
                                + " WHERE MBM_ITFLG = '3' "
                                + " AND MBM_PROCD = '" + code + "' "
                                + " ORDER BY MBM_SEQNO ");
                            DT_MISC = dbHlpr.FetchData("SELECT "
                                + " MBM_ITMNO AS ITMCOD, BLG_NAME AS ITMDESC1, COALESCE(MPL_QTY, 0.000) AS ITMQTY "
                                + " FROM BOM_MST_MRNTN "
                                + " LEFT JOIN STP_MSTR_BOMLG ON MBM_ITMNO = BLG_CODE "
                                + " LEFT JOIN PRC_TRX_MRNLN ON MBM_ITMNO = MPL_ITMCD AND MPL_TYPE = 'MSC' AND MPL_ID = '" + prcs + "' "
                                + " WHERE MBM_ITFLG = '4' "
                                + " AND MBM_PROCD = '" + code + "' "
                                + " ORDER BY MBM_SEQNO ");
                        }

                        return Json(new
                        {
                            success = true,
                            rmData = new { rmCode = code, rmDesc1 = desc1, rmDesc2 = desc2, rmUom = uom },
                            umtTblData = JsonConvert.SerializeObject(DT_MEAT),
                            ingTblData = JsonConvert.SerializeObject(DT_INGRDNTS),
                            mscTblData = JsonConvert.SerializeObject(DT_MISC),
                            prcsId = processId
                        });
                    }
                    else
                    {
                        return Json(new
                        {
                            success = false,
                            msg = "Error :: Item not found."
                        });
                    }
                } // end if RMT code is null or empty
                else
                {
                    return Json(new
                    {
                        success = false,
                        msg = "Error :: Please select a Raw Meat item to save."
                    });
                } // end if RMT code is null or empty
            } // end if sender is RMT
            else if (sndr.ToLower() == "sdate")
            {
                string processId = "";
                try
                {
                    processId = ProductionModel.GetNextProcessId(code, Convert.ToDateTime(prcs));
                }
                catch (Exception)
                {
                    processId = "";
                }

                return Json(new
                {
                    success = true,
                    prcsId = processId
                });
            }

            return Json(new
            {
                success = false,
                msg = "Error :: Invalid Request."
            });
        }

        [HttpPost]
        public ActionResult UMTProcessDropDown(string code)
        {
            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            DataTable dtYldngs = dbHlpr.FetchData("SELECT PRL_ID, PRL_ITMCD, PRL_GRVNO "
                + " FROM PRC_HST_YLDLN "
                + " WHERE PRL_ITMCD = '" + code + "' AND PRL_TYPE = 'UMT' "
                + " ORDER BY PRL_SDATE DESC, PRL_BCHNO DESC ");

            return Json(new { success = true, data = JsonConvert.SerializeObject(dtYldngs) });
        }

        [HttpPost]
        public ActionResult UMTGrvDropDown(string code)
        {
            DataTable dtRmt = RawMaterial.GetByCode(code);

            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            DataTable dtGrvs = new DataTable();
            if (dtRmt.Rows.Count > 0)
            {
                dtGrvs = dbHlpr.FetchData("SELECT "
                        + " RCV_FMSGRV, RCV_RMCOD "
                        + " FROM INV_GRV_RCVNG "
                        + " WHERE RCV_RMCOD = '" + dtRmt.Rows[0]["RMT_LNKCD"].ToString() + "' "
                        + " ORDER BY RCV_FMSGRV DESC ");
            }

            return Json(new { success = true, data = JsonConvert.SerializeObject(dtGrvs) });
        }

        [NonAction]
        private KeyValuePair<bool, string> ValidateProcessData(FormCollection f, bool ValidateForPosting)
        {
            string msg = "";
            if (f["prod_cd"] == null || f["prod_cd"].ToString().Length <= 0)
            {
                msg = "Item is required";
                return new KeyValuePair<bool, string>(false, msg);
            }

            DateTime startdate;
            if (f["bch_start_dt"] == null || !DateTime.TryParse(f["bch_start_dt"].ToString(), out startdate))
            {
                msg = "Invalid Start Date";
                return new KeyValuePair<bool, string>(false, msg);
            }
            DateTime enddate;
            if (f["bch_start_dt"] == null || !DateTime.TryParse(f["bch_end_dt"].ToString(), out enddate))
            {
                msg = "Invalid End Date";
                return new KeyValuePair<bool, string>(false, msg);
            }

            if (startdate > enddate)
            {
                msg = "End date cannot be before Start Date";
                return new KeyValuePair<bool, string>(false, msg);
            }

            if (f["prcs_id"] == "-" || f["prcs_id"].Trim().Length < 5)
            {
                msg = "Invalid Process ID";
                return new KeyValuePair<bool, string>(false, msg);
            }

            string[] UMT_ITMS = f["umt_itmcds[]"] == null ? new string[] { } : f["umt_itmcds[]"].Split(',');
            string[] UMT_PROCS = f["umt_procsids[]"] == null ? new string[] { } : f["umt_procsids[]"].Split(',');
            string[] UMT_GRVS = f["umt_grvs[]"] == null ? new string[] { } : f["umt_grvs[]"].Split(',');
            string[] UMT_QTYS = f["umt_qtys[]"] == null ? new string[] { } : f["umt_qtys[]"].Split(',');
            if (UMT_GRVS.Length <= 0 || UMT_QTYS.Length <= 0 || UMT_ITMS.Length != UMT_QTYS.Length)
            {
                msg = "Raw Meat details are required. Please enter GRV and Quantity";
                return new KeyValuePair<bool, string>(false, msg);
            }

            string umtGridErrors = "";
            for (int i = 0; i < UMT_ITMS.Length; i++)
            {
                if (UMT_GRVS[i].ToString().Length <= 0 && UMT_PROCS[i].ToString().Length <= 0)
                {
                    umtGridErrors += "Meat GRV # or Batch ID is required for all rows.<br />";
                }

                if (UMT_QTYS[i].ToString().Length <= 0)
                {
                    umtGridErrors += "Item Quantity is required for all rows<br />";
                }
            }
            if (umtGridErrors.Trim().Length > 0)
            {
                msg = umtGridErrors;
                return new KeyValuePair<bool, string>(false, msg);
            }

            string[] ING_ITMS = f["ing_itmcds[]"] == null ? new string[] { } : f["ing_itmcds[]"].Split(',');
            string[] ING_GRVS = f["ing_grvs[]"] == null ? new string[] { } : f["ing_grvs[]"].Split(',');
            string[] ING_QTYS = f["ing_qtys[]"] == null ? new string[] { } : f["ing_qtys[]"].Split(',');
            string ingGridErrors = "";
            for (int i = 0; i < ING_ITMS.Length; i++)
            {
                if (ValidateForPosting)
                {
                    if (ING_GRVS[i].ToString().Length <= 0)
                    {
                        ingGridErrors += "Ingredients GRV # is required for all rows.<br />";
                    }
                }
                if (ING_QTYS[i].ToString().Length <= 0)
                {
                    ingGridErrors += "Ingredient Quantity is required for all rows.<br />";
                }
            }
            if (ingGridErrors.Trim().Length > 0)
            {
                msg = ingGridErrors;
                return new KeyValuePair<bool, string>(false, msg);
            }

            string[] MSC_ITMS = f["msc_itmcds[]"] == null ? new string[] { } : f["msc_itmcds[]"].Split(',');
            string[] MSC_QTYS = f["msc_qtys[]"] == null ? new string[] { } : f["msc_qtys[]"].Split(',');
            string miscGridErrors = "";
            for (int i = 0; i < MSC_ITMS.Length; i++)
            {
                if (MSC_QTYS[i].ToString().Length <= 0)
                {
                    miscGridErrors += "Misc. Quantity is required for all rows<br />";
                }
            }
            if (miscGridErrors.Trim().Length > 0)
            {
                msg = miscGridErrors;
                return new KeyValuePair<bool, string>(false, msg);
            }

            return new KeyValuePair<bool, string>(true, "");
        }

        [NonAction]
        private KeyValuePair<bool, List<dynamic>> ValidateYieldQuantities(FormCollection f)
        {
            string[] UMT_ITMS = f["umt_itmcds[]"] == null ? new string[] { } : f["umt_itmcds[]"].Split(',');
            string[] UMT_PROCSIDS = f["umt_procsids[]"] == null ? new string[] { } : f["umt_procsids[]"].Split(',');
            string[] UMT_GRVS = f["umt_grvs[]"] == null ? new string[] { } : f["umt_grvs[]"].Split(',');
            string[] UMT_QTYS = f["umt_qtys[]"] == null ? new string[] { } : f["umt_qtys[]"].Split(',');

            string lblProcessId = f["prcs_id"];
            for (int i = 0; i < UMT_ITMS.Length; i++)
            {
                if (UMT_PROCSIDS[i].ToString().Length > 0)
                {
                    float qty;
                    if (float.TryParse(UMT_QTYS[i].ToString(), out qty))
                    {
                        string yldProcessId = UMT_PROCSIDS[i].ToString();
                        string itmCd = UMT_ITMS[i].ToString();

                        DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
                        DataTable yldQtyData = dbHlpr.FetchData("SELECT COALESCE(SUM(PRL_QTY), 0) FROM PRC_HST_YLDLN WHERE PRL_ITMCD = '" + itmCd + "' AND PRL_ID = '" + yldProcessId + "' ");
                        float yldQty = float.Parse(yldQtyData.Rows[0][0].ToString());

                        DataTable usageData = dbHlpr.FetchData("SELECT * FROM ( "
                            + " SELECT MPL_PROCD, MPL_ID, MPL_ITMCD, SUM(MPL_QTY) AS QTY, 'HST' AS TRXTYP "
                            + " FROM PRC_HST_MRNLN "
                            + " WHERE MPL_ITMCD = '" + itmCd + "' AND MPL_PRCID = '" + yldProcessId + "' "
                            + " AND MPL_ID <> '" + lblProcessId + "' "
                            + " GROUP BY MPL_PROCD, MPL_ID, MPL_ITMCD "

                            + " UNION ALL "

                            + " SELECT MPL_PROCD, MPL_ID, MPL_ITMCD, SUM(MPL_QTY) AS QTY, 'TRX' AS TRXTYP "
                            + " FROM PRC_TRX_MRNLN "
                            + " WHERE MPL_ITMCD = '" + itmCd + "' AND MPL_PRCID = '" + yldProcessId + "' "
                            + " AND MPL_ID <> '" + lblProcessId + "' "
                            + " GROUP BY MPL_PROCD, MPL_ID, MPL_ITMCD "
                            + " ) tbl "
                            + " ORDER BY MPL_ID, MPL_ITMCD ");

                        float totalQtyUsedInOtherProcesses = 0;
                        if (usageData.Rows.Count > 0)
                        {
                            totalQtyUsedInOtherProcesses = float.Parse(usageData.Compute("Sum(QTY)", string.Empty).ToString());
                        }

                        float thisProcessQty = GetYieldUsageInCurrentProcess(itmCd, yldProcessId, UMT_ITMS, UMT_PROCSIDS, UMT_QTYS);

                        float totalQtyUsed = totalQtyUsedInOtherProcesses + thisProcessQty;
                        if (Math.Round(totalQtyUsed, 3, MidpointRounding.AwayFromZero) > Math.Round(yldQty, 3, MidpointRounding.AwayFromZero))
                        {
                            List<dynamic> lst = new List<dynamic>();
                            lst.Add(yldQty);
                            lst.Add(thisProcessQty);
                            lst.Add(itmCd);
                            lst.Add(usageData);
                            return new KeyValuePair<bool, List<dynamic>>(false, lst);
                        }
                    }
                }
            }

            return new KeyValuePair<bool, List<dynamic>>(true, new List<dynamic>());
        }

        [NonAction]
        private float GetYieldUsageInCurrentProcess(string itm, string prcs, string[] ItemCodes, string[] ProcessIds, string[] Quantities)
        {
            float qty = 0;

            for (int i = 0; i < ItemCodes.Length; i++)
            {
                if (ItemCodes[i] == itm && ProcessIds[i] == prcs)
                {
                    try
                    {
                        qty += float.Parse(Quantities[i]);
                    }
                    catch (Exception x) { }
                }
            }

            return qty;
        }

        [HttpPost]
        public ActionResult SaveProcess(FormCollection form)
        {
            KeyValuePair<bool, string> validateData = this.ValidateProcessData(form, false);
            if (validateData.Key)
            {
                KeyValuePair<bool, List<dynamic>> validateYieldData = this.ValidateYieldQuantities(form);
                if (!validateYieldData.Key)
                {
                    List<dynamic> lst = validateYieldData.Value;
                    string yldQty = lst[0].ToString();
                    string thisProcessQty = lst[1].ToString();
                    string itmCd = lst[2].ToString();
                    DataTable usageData = (DataTable)lst[3];
                    return Json(new
                    {
                        success = false,
                        msg = "Lean Meat Quantities Mis-match",
                        yldQty = yldQty,
                        thisProcessQty = thisProcessQty,
                        itmCd = itmCd,
                        usageData = JsonConvert.SerializeObject(usageData)
                    });
                }

                DateTime dtNow = DateTime.Now;

                string[] UMT_ITMS = form["umt_itmcds[]"] == null ? new string[] { } : form["umt_itmcds[]"].Split(',');
                string[] UMT_PROCSIDS = form["umt_procsids[]"] == null ? new string[] { } : form["umt_procsids[]"].Split(',');
                string[] UMT_GRVS = form["umt_grvs[]"] == null ? new string[] { } : form["umt_grvs[]"].Split(',');
                string[] UMT_QTYS = form["umt_qtys[]"] == null ? new string[] { } : form["umt_qtys[]"].Split(',');

                string[] ING_ITMS = form["ing_itmcds[]"] == null ? new string[] { } : form["ing_itmcds[]"].Split(',');
                string[] ING_GRVS = form["ing_grvs[]"] == null ? new string[] { } : form["ing_grvs[]"].Split(',');
                string[] ING_QTYS = form["ing_qtys[]"] == null ? new string[] { } : form["ing_qtys[]"].Split(',');

                string[] MSC_ITMS = form["msc_itmcds[]"] == null ? new string[] { } : form["msc_itmcds[]"].Split(',');
                string[] MSC_QTYS = form["msc_qtys[]"] == null ? new string[] { } : form["msc_qtys[]"].Split(',');

                double totalMeatWeight = 0;
                double totalIngrdntsWeight = 0;
                for (int i = 0; i < UMT_ITMS.Length; i++)
                {
                    double qty;
                    if (double.TryParse(UMT_QTYS[i].ToString(), out qty))
                    {
                        totalMeatWeight = totalMeatWeight + qty;
                    }
                }
                for (int i = 0; i < ING_ITMS.Length; i++)
                {
                    double qty;
                    if (double.TryParse(ING_QTYS[i].ToString(), out qty))
                    {
                        totalIngrdntsWeight = totalIngrdntsWeight + qty;
                    }
                }

                if (totalMeatWeight <= 0)
                {
                    return Json(new { success = false, msg = "Item total quantity must be greater than Zero." });
                }

                double TotalBatchWeight = 0;
                double TotalPreparedQty = 0;
                double TotalMincerLoss = 0;
                double TotalQcQty = 0;
                bool ValidateForPreparedWeight = false;
                for (int i = 0; i < MSC_ITMS.Length; i++)
                {
                    if (MSC_ITMS[i].ToString() == "MS-9961")
                    {
                        try
                        {
                            double.TryParse(MSC_QTYS[i].ToString(), out TotalBatchWeight);
                        }
                        catch (Exception ex) { }
                    }
                    else if (MSC_ITMS[i].ToString() == "MS-9962")
                    {
                        ValidateForPreparedWeight = true;
                        try
                        {
                            double.TryParse(MSC_QTYS[i].ToString(), out TotalPreparedQty);
                        }
                        catch (Exception ex) { }
                    }
                    else if (MSC_ITMS[i].ToString() == "MS-9963")
                    {
                        ValidateForPreparedWeight = true;
                        try
                        {
                            double.TryParse(MSC_QTYS[i].ToString(), out TotalMincerLoss);
                        }
                        catch (Exception ex) { }
                    }
                    else if (MSC_ITMS[i].ToString() == "MS-9966")
                    {
                        ValidateForPreparedWeight = true;
                        try
                        {
                            double.TryParse(MSC_QTYS[i].ToString(), out TotalQcQty);
                        }
                        catch (Exception ex) { }
                    }
                }

                if (Math.Round((totalMeatWeight + totalIngrdntsWeight), 3, MidpointRounding.AwayFromZero) != Math.Round(TotalBatchWeight, 3, MidpointRounding.AwayFromZero))
                {
                    return Json(new { success = false, msg = "Difference of \"" + ((totalMeatWeight + totalIngrdntsWeight) - TotalBatchWeight).ToString("n3") + "\" in Total Batch weight and Total Input Qty." });
                }

                if (ValidateForPreparedWeight && Math.Round((TotalPreparedQty + TotalMincerLoss + TotalQcQty), 3, MidpointRounding.AwayFromZero) != Math.Round(TotalBatchWeight, 3, MidpointRounding.AwayFromZero))
                {
                    return Json(new { success = false, msg = "Total Batch Weight should match Total Prepared Qty and Mincer Loss. Difference of \"" + ((TotalPreparedQty + TotalMincerLoss + TotalQcQty) - TotalBatchWeight).ToString("n3") + "\" found" });
                }

                string ProdCd = form["prod_cd"];
                string ProdDesc = Product.GetByCode(ProdCd).Rows[0]["PRO_DESC1"].ToString().Trim();
                string RefNo = form["ref_no"].Trim();
                DateTime deBatchDate = Convert.ToDateTime(form["bch_start_dt"]);
                string StartDate = Convert.ToDateTime(form["bch_start_dt"]).ToString("yyyy-MM-dd");
                string StartTime = "00:00:00"; // Convert.ToDateTime(form["bch_start_tm"]).ToString("HH:mm:ss");
                string EndDate = Convert.ToDateTime(form["bch_end_dt"]).ToString("yyyy-MM-dd");
                string EndTime = "00:00:00"; // Convert.ToDateTime(form["bch_end_tm"]).ToString("HH:mm:ss");
                string weekToPut = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(new DateTime(deBatchDate.Year, deBatchDate.Month, deBatchDate.Day), CalendarWeekRule.FirstDay, DayOfWeek.Saturday).ToString();
                string lblProcessId = form["prcs_id"];

                DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
                dbHlpr.ExecuteNonQuery("DELETE FROM PRC_TRX_MRNHD WHERE MPH_ID = '" + lblProcessId + "'");
                dbHlpr.ExecuteNonQuery("DELETE FROM PRC_TRX_MRNLN WHERE MPL_ID = '" + lblProcessId + "'");

                string bchCode = lblProcessId;
                string bchNo = lblProcessId.Substring(lblProcessId.IndexOf('-') + 1);

                string hdrQry = "INSERT INTO PRC_TRX_MRNHD ( "
                    + " MPH_ID, MPH_BCHNO, MPH_PROCD, MPH_DESC1, "
                    + " MPH_SDATE, MPH_STIME, MPH_EDATE, MPH_ETIME, "
                    + " MPH_WEEK, MPH_WTKGS, MPH_REFNO, MPH_ENTBY, MPH_ENTDT, "
                    + " MPH_ENTIM "
                    + " ) VALUES ( "
                    + " '" + bchCode + "', '" + bchNo + "', '" + ProdCd + "', '" + ProdDesc + "', "
                    + " '" + StartDate + "', '" + StartTime + "', '" + EndDate + "', '" + EndTime + "', "
                    + " '" + weekToPut + "', '" + totalMeatWeight + "', '" + RefNo + "', '" + Session["UserId"] + "', '" + dtNow + "', '" + dtNow + "' "
                    + " ) ";
                dbHlpr.ExecuteNonQuery(hdrQry);

                for (int i = 0; i < UMT_ITMS.Length; i++)
                {
                    string itmCode = UMT_ITMS[i].ToString();
                    string itmDesc = RawMaterial.GetByCode(UMT_ITMS[i]).Rows[0]["RMT_DESC1"].ToString();
                    string itmGrv = UMT_GRVS[i].ToString();
                    string itmProcs = UMT_PROCSIDS[i].ToString();
                    string itmQty = UMT_QTYS[i].ToString();

                    string rmLinQry = "INSERT INTO PRC_TRX_MRNLN ( "
                        + " MPL_ID, MPL_BCHNO, "
                        + " MPL_SDATE, MPL_STIME, MPL_EDATE, MPL_ETIME, "
                        + " MPL_WEEK, MPL_PROCD, MPL_WTKGS, "
                        + " MPL_ITMCD, MPL_DESC1, MPL_TYPE, MPL_GRVNO, MPL_PRCID, MPL_QTY, "
                        + " MPL_ENTBY, MPL_ENTDT, MPL_ENTIM "
                        + " ) VALUES ( "
                        + " '" + bchCode + "', '" + bchNo + "', "
                        + " '" + StartDate + "', '" + StartTime + "', '" + EndDate + "', '" + EndTime + "', "
                        + " '" + weekToPut + "', '" + ProdCd + "', '" + totalMeatWeight + "', "
                        + " '" + itmCode + "', '" + itmDesc + "', 'UMT', '" + itmGrv + "', '" + itmProcs + "', '" + itmQty + "', "
                        + " '" + Session["UserId"] + "', '" + dtNow + "', '" + dtNow + "' "
                        + " ) ";
                    dbHlpr.ExecuteNonQuery(rmLinQry);
                }

                // Usable Meat
                for (int i = 0; i < ING_ITMS.Length; i++)
                {
                    string itmCode = ING_ITMS[i].ToString();
                    string itmDesc = RawMaterial.GetByCode(ING_ITMS[i]).Rows[0]["RMT_DESC1"].ToString();
                    string itmGrv = ING_GRVS[i].ToString();
                    string itmQty = ING_QTYS[i].ToString();

                    string umLinQry = "INSERT INTO PRC_TRX_MRNLN ("
                        + " MPL_ID, MPL_BCHNO, "
                        + " MPL_SDATE, MPL_STIME, MPL_EDATE, MPL_ETIME, "
                        + " MPL_WEEK, MPL_PROCD, MPL_WTKGS, "
                        + " MPL_ITMCD, MPL_DESC1, MPL_TYPE, MPL_GRVNO, MPL_QTY, "
                        + " MPL_ENTBY, MPL_ENTDT, MPL_ENTIM "
                        + " ) VALUES ( "
                        + " '" + bchCode + "', '" + bchNo + "', "
                        + " '" + StartDate + "', '" + StartTime + "', '" + EndDate + "', '" + EndTime + "', "
                        + " '" + weekToPut + "', '" + ProdCd + "', '" + totalMeatWeight + "', "
                        + " '" + itmCode + "', '" + itmDesc + "', 'ING', '" + itmGrv + "', '" + itmQty + "', "
                        + " '" + Session["UserId"] + "', '" + dtNow + "', '" + dtNow + "' "
                        + " ) ";
                    dbHlpr.ExecuteNonQuery(umLinQry);
                }

                // Miscellaneous
                for (int i = 0; i < MSC_ITMS.Length; i++)
                {
                    string itmCode = MSC_ITMS[i].ToString();
                    string itmDesc = BomLegend.GetByCode(MSC_ITMS[i]).Rows[0]["BLG_NAME"].ToString();
                    string itmGrv = "";
                    string itmQty = MSC_QTYS[i].ToString();

                    string wsLinQry = "INSERT INTO PRC_TRX_MRNLN ("
                        + " MPL_ID, MPL_BCHNO, "
                        + " MPL_SDATE, MPL_STIME, MPL_EDATE, MPL_ETIME, "
                        + " MPL_WEEK, MPL_PROCD, MPL_WTKGS, "
                        + " MPL_ITMCD, MPL_DESC1, MPL_TYPE, MPL_GRVNO, MPL_QTY, "
                        + " MPL_ENTBY, MPL_ENTDT, MPL_ENTIM "
                        + " ) VALUES ( "
                        + " '" + bchCode + "', '" + bchNo + "', "
                        + " '" + StartDate + "', '" + StartTime + "', '" + EndDate + "', '" + EndTime + "', "
                        + " '" + weekToPut + "', '" + ProdCd + "', '" + totalMeatWeight + "', "
                        + " '" + itmCode + "', '" + itmDesc + "', 'MSC', '" + itmGrv + "', '" + itmQty + "', "
                        + " '" + Session["UserId"] + "', '" + dtNow + "', '" + dtNow + "' "
                        + " ) ";
                    dbHlpr.ExecuteNonQuery(wsLinQry);
                }

                SetSuccessMessage("Process Saved Successfully");
                return Json(new { success = true, url = Url.Action("ProcessInquiry", "Production") });
            }

            return Json(new { success = false, msg = validateData.Value });
        }


        public ActionResult PostProcess(FormCollection form)
        {
            KeyValuePair<bool, string> validateData = this.ValidateProcessData(form, true);
            if (!validateData.Key || form["posting"] != "true")
            {
                return Json(new { success = false, msg = validateData.Value });
            }
            KeyValuePair<bool, List<dynamic>> validateYieldData = this.ValidateYieldQuantities(form);
            if (!validateYieldData.Key)
            {
                List<dynamic> lst = validateYieldData.Value;
                string yldQty = lst[0].ToString();
                string thisProcessQty = lst[1].ToString();
                string itmCd = lst[2].ToString();
                DataTable usageData = (DataTable)lst[3];
                return Json(new
                {
                    success = false,
                    msg = "Lean Meat Quantities Mis-match",
                    yldQty = yldQty,
                    thisProcessQty = thisProcessQty,
                    itmCd = itmCd,
                    usageData = JsonConvert.SerializeObject(usageData)
                });
            }

            string ProdCd = form["prod_cd"];
            string lblProcessId = form["prcs_id"];

            DateTime dateTimeNow = DateTime.Now;
            string dateTimeNowDate = dateTimeNow.ToString("yyyy-MM-dd");
            string dateTimeNowTime = dateTimeNow.ToString("HH:mm:ss.fff");

            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            dbHlpr.ExecuteScalarQuery("INSERT INTO PRC_HST_MRNHD ( "
                + " MPH_ID, MPH_BCHNO, MPH_PROCD, "
                + " MPH_DESC1, MPH_SDATE, MPH_STIME, "
                + " MPH_EDATE, MPH_ETIME, MPH_WEEK, "
                + " MPH_WTKGS, MPH_REFNO, MPH_ENTBY, MPH_ENTDT, "
                + " MPH_ENTIM, MPH_UPDBY, MPH_LSUPD, MPH_LSUPT, "
                + " MPH_PSTBY, MPH_PSTDT, MPH_PSTIM "
                + " ) SELECT "
                + " MPH_ID, MPH_BCHNO, MPH_PROCD, "
                + " MPH_DESC1, MPH_SDATE, MPH_STIME, "
                + " MPH_EDATE, MPH_ETIME, MPH_WEEK, "
                + " MPH_WTKGS, MPH_REFNO, MPH_ENTBY, MPH_ENTDT, "
                + " MPH_ENTIM, MPH_UPDBY, MPH_LSUPD, MPH_LSUPT, "
                + " '" + Session["UserId"] + "', '" + dateTimeNowDate + "', '" + dateTimeNowTime + "' "
                + " FROM PRC_TRX_MRNHD "
                + " WHERE PRC_TRX_MRNHD.MPH_ID = '" + lblProcessId.Trim() + "'");

            dbHlpr.ExecuteScalarQuery("INSERT INTO PRC_HST_MRNLN ( "
                + " MPL_ID, MPL_BCHNO, MPL_SDATE, "
                + " MPL_STIME, MPL_EDATE, MPL_ETIME, "
                + " MPL_WEEK, MPL_PROCD, MPL_WTKGS, "
                + " MPL_ITMCD, MPL_DESC1, MPL_TYPE, "
                + " MPL_GRVNO, MPL_PRCID, MPL_QTY, MPL_ENTBY, "
                + " MPL_ENTDT, MPL_ENTIM, MPL_UPDBY, "
                + " MPL_LSUPD, MPL_LSUPT, "
                + " MPL_PSTBY, MPL_PSTDT, MPL_PSTIM "
                + " ) SELECT "
                + " MPL_ID, MPL_BCHNO, MPL_SDATE, "
                + " MPL_STIME, MPL_EDATE, MPL_ETIME, "
                + " MPL_WEEK, MPL_PROCD, MPL_WTKGS, "
                + " MPL_ITMCD, MPL_DESC1, MPL_TYPE, "
                + " MPL_GRVNO, MPL_PRCID, MPL_QTY, MPL_ENTBY, "
                + " MPL_ENTDT, MPL_ENTIM, MPL_UPDBY, "
                + " MPL_LSUPD, MPL_LSUPT, "
                + " '" + Session["UserId"] + "', '" + dateTimeNowDate + "', '" + dateTimeNowTime + "' "
                + " FROM PRC_TRX_MRNLN "
                + " WHERE PRC_TRX_MRNLN.MPL_ID = '" + lblProcessId.Trim() + "'");

            dbHlpr.ExecuteScalarQuery("DELETE FROM PRC_TRX_MRNHD WHERE MPH_ID = '" + lblProcessId.Trim() + "' ");
            dbHlpr.ExecuteScalarQuery("DELETE FROM PRC_TRX_MRNLN WHERE MPL_ID = '" + lblProcessId.Trim() + "' ");

            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            string comments = "Posted MCPU Production Process Batch for "
                + ProdCd.ToString() + " and Batch : '" + lblProcessId.Trim() + "' into "
                + controllerName + "/" + actionName + " form at " + System.DateTime.Now.ToString();
            string extras = "PRCS of RM Code " + ProdCd + " & '" + lblProcessId.Trim() + "' Posted";
            dbHlpr.InsertUserLog(Session["UserId"].ToString(), Session["UserName"].ToString(), DateTime.Now, comments, extras);

            SetSuccessMessage("Production Process Posted Successfully.");

            return Json(new { success = true, url = Url.Action("ProcessInquiry") });
        }   
    }
}