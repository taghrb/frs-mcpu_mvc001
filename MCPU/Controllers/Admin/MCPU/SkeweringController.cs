﻿using MCPU.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MCPU.Controllers.Admin.MCPU
{
    public class SkeweringController : AuthController
    {
        string PROCESS_VIEW = "~/Views/Admin/MCPU/Skewering/Process.cshtml";
        string PROCESSINQUIRY_VIEW = "~/Views/Admin/MCPU/Skewering/ProcessInquiry.cshtml";

        public ActionResult ProcessInquiry()
        {
            DataTable dt = Product.GetAllHavingProductionBom();

            ViewBag.PRDCTS = new SelectList(dt.AsDataView(), "PRO_CODE", "PRO_CODE");

            return View(PROCESSINQUIRY_VIEW);
        }

        [HttpPost]
        public ActionResult GetOpenProcessesByProd(string code)
        {
            DataTable dt = Product.GetByCode(code);

            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            DataTable dtData = dbHlpr.FetchData("SELECT "
                        + " SPH_ID, SPH_BCHNO, SPH_PROCD, SPH_DESC1, "
                        + " SPH_SDATE, SPH_STIME, SPH_EDATE, SPH_ETIME, "
                        + " SPH_WEEK, SPH_WTKGS, SPH_ENTBY, SPH_ENTDT, SPH_ENTIM, "
                        + " SPH_UPDBY, SPH_LSUPD, SPH_LSUPT "
                        + " FROM PRC_TRX_SKWHD "
                        + " WHERE SPH_PROCD = '" + code + "' ");

            return Json(new { success = true, data = JsonConvert.SerializeObject(dt), processes = JsonConvert.SerializeObject(dtData) });
        }

        public ActionResult Process(string prod, string prcs)
        {
            DataTable dtProd = Product.GetAllHavingProductionBom();
            ViewBag.PRDCTS = new SelectList(dtProd.AsDataView(), "PRO_CODE", "PRO_CODE");

            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            DataTable dtRmtAndBom = dbHlpr.FetchData("SELECT BLG_CODE, BLG_NAME FROM ("
                                    + " SELECT BLG_CODE, BLG_NAME "
                                    + " FROM STP_MSTR_BOMLG "
                                    + " UNION ALL "
                                    + " SELECT RMT_CODE AS BLG_CODE, RMT_DESC1 AS BLG_NAME "
                                    + " FROM INV_MSTR_RWMAT) a "
                                    + " ORDER BY a.BLG_CODE ");
            ViewBag.RMTS_AND_MISC = new SelectList(dtRmtAndBom.AsDataView(), "BLG_CODE", "BLG_CODE");

            ViewBag.BatchStartDateEnabled = true;
            ViewBag.BatchStartDate = "";
            ViewBag.BatchEndDate = "";
            ViewBag.BatchStartTime = "";
            ViewBag.BatchEndTime = "";
            ViewBag.ProcessId = "";
            ViewBag.ProdCodeReadOnly = false;
            ViewBag.RefNo = "";
            ViewBag.PostEnabled = false;
            if (prod != null && prcs != null && prod != "" && prcs != "")
            {

                DataTable ProductionDetails = SkeweringModel.GetProcessHeader(prod, prcs);
                if (ProductionDetails.Rows.Count > 0)
                {
                    ViewBag.PRDCTS = new SelectList(dtProd.AsDataView(), "PRO_CODE", "PRO_CODE", prod);
                    ViewBag.ProdCode = prod;
                    ViewBag.ProcessId = prcs;
                    ViewBag.PostEnabled = true;
                    ViewBag.BatchStartDateEnabled = false;

                    try
                    {
                        ViewBag.BatchStartDate = Convert.ToDateTime(ProductionDetails.Rows[0]["SPH_SDATE"].ToString()).ToString("dd-MMM-yyyy");
                    }
                    catch (Exception ex)
                    {
                        ViewBag.BatchStartDate = "";
                    }

                    try
                    {
                        ViewBag.BatchEndDate = Convert.ToDateTime(ProductionDetails.Rows[0]["SPH_EDATE"].ToString()).ToString("dd-MMM-yyyy");
                    }
                    catch (Exception ex)
                    {
                        ViewBag.BatchEndDate = "";
                    }

                    try
                    {
                        ViewBag.BatchStartTime = Convert.ToDateTime(ProductionDetails.Rows[0]["SPH_STIME"].ToString()).ToString("HH:mm");
                    }
                    catch (Exception ex)
                    {
                        ViewBag.BatchStartTime = "00:00";
                    }

                    try
                    {
                        ViewBag.BatchEndTime = Convert.ToDateTime(ProductionDetails.Rows[0]["SPH_ETIME"].ToString()).ToString("HH:mm");
                    }
                    catch (Exception ex)
                    {
                        ViewBag.BatchEndTime = "00:00";
                    }

                    ViewBag.ProdCodeReadOnly = true;
                    ViewBag.RefNo = ProductionDetails.Rows[0]["SPH_REFNO"].ToString();
                }
            }

            return View(PROCESS_VIEW);
        }

        [HttpPost]
        public ActionResult LoadProcess(string code, string prcs, string sndr, bool prodRdOnly)
        {
            if (sndr.ToLower() == "prod")
            {
                DataTable DT_MISC = new DataTable();

                string desc1 = "";
                string desc2 = "";
                string uom = "";

                string processId = "";
                try
                {
                    processId = SkeweringModel.GetNextProcessId(code, Convert.ToDateTime(prcs));
                }
                catch (Exception)
                {
                    processId = prcs;
                }

                if (code != null)
                {
                    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

                    DataTable dtMarinationBatches = dbHlpr.FetchData("SELECT DISTINCT MPL_ID FROM PRC_HST_MRNLN WHERE MPL_PROCD = '" + code + "' ORDER BY MPL_ID DESC ");

                    DataTable dt = dbHlpr.FetchData("SELECT "
                        + " PRO_CODE, PRO_DESC1, PRO_DESC2, PRO_TRKMD, PRO_CATG, PRO_SCATG, "
                        + " PRO_UDF1, PRO_UDF2, PRO_UDF3, PRO_UDF4, PRO_UOM, PRO_WT, "
                        + " PRO_ALCOD, PRO_STATS "
                        + " FROM INV_MSTR_PRODT "
                        + " WHERE PRO_CODE = '" + code + "' ");
                    if (dt.Rows.Count > 0)
                    {
                        desc1 = dt.Rows[0]["PRO_DESC1"].ToString();
                        desc2 = dt.Rows[0]["PRO_DESC2"].ToString();
                        uom = dt.Rows[0]["PRO_UOM"].ToString();

                        if (prodRdOnly)
                        {
                            DT_MISC = dbHlpr.FetchData("SELECT "
                            + " SPL_PRCID AS ITMPROCSID, SPL_GRVNO AS ITMGRV, SPL_ITMCD AS ITMCOD, BLG_NAME AS ITMDESC1, COALESCE(SPL_QTY, 0.000) AS ITMQTY "
                            + " FROM PRC_TRX_SKWLN "
                            + " LEFT JOIN STP_MSTR_BOMLG ON SPL_ITMCD = BLG_CODE "
                            + " WHERE SPL_TYPE = 'MSC' "
                            + " AND SPL_PROCD = '" + code + "' AND SPL_ID = '" + prcs + "' "

                            + " UNION ALL "

                            + " SELECT "
                            + " SPL_PRCID AS ITMPROCSID, SPL_GRVNO AS ITMGRV, SPL_ITMCD AS ITMCOD, RMT_DESC1 AS ITMDESC1, COALESCE(SPL_QTY, 0.000) AS ITMQTY "
                            + " FROM PRC_TRX_SKWLN "
                            + " LEFT JOIN INV_MSTR_RWMAT ON SPL_ITMCD = RMT_CODE "
                            + " WHERE SPL_TYPE = 'RMT' "
                            + " AND SPL_PROCD = '" + code + "' AND SPL_ID = '" + prcs + "' ");
                        }
                        else
                        {
                            DT_MISC = dbHlpr.FetchData("SELECT '' AS ITMPROCSID, '' AS ITMGRV, '' AS ITMCOD, '' AS ITMDESC1, 0.000 AS ITMQTY ");
                        }

                        return Json(new
                        {
                            success = true,
                            rmData = new { rmCode = code, rmDesc1 = desc1, rmDesc2 = desc2, rmUom = uom },
                            mscTblData = JsonConvert.SerializeObject(DT_MISC),
                            batchCodes = JsonConvert.SerializeObject(dtMarinationBatches),
                            prcsId = processId,
                            batchId = DT_MISC.Rows.Count > 0 ? DT_MISC.Rows[0]["ITMPROCSID"].ToString() : ""
                        });
                    }
                    else
                    {
                        return Json(new
                        {
                            success = false,
                            msg = "Error :: Item not found."
                        });
                    }
                } // end if RMT code is null or empty
                else
                {
                    return Json(new
                    {
                        success = false,
                        msg = "Error :: Please select a Raw Meat item to save."
                    });
                } // end if RMT code is null or empty
            } // end if sender is RMT
            else if (sndr.ToLower() == "sdate")
            {
                string processId = "";
                try
                {
                    processId = SkeweringModel.GetNextProcessId(code, Convert.ToDateTime(prcs));
                }
                catch (Exception)
                {
                    processId = "";
                }

                return Json(new
                {
                    success = true,
                    prcsId = processId
                });
            }

            return Json(new
            {
                success = false,
                msg = "Error :: Invalid Request."
            });
        }

        [HttpPost]
        public ActionResult GetProductionQtys(string prodCd, string batchCd)
        {
            string lblMeatQty = "0.000";
            string lblIngrdntsQty = "0.000";
            string lblTotalQty = "0.000";

            if (batchCd != null && prodCd != null)
            {
                if (batchCd.ToString() != "" && prodCd.ToString() != "")
                {
                    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
                    DataTable dtMeatQty = dbHlpr.FetchData("SELECT "
                        + " SUM(MPL_QTY) AS TOT_QTY "
                        + " FROM PRC_HST_MRNLN "
                        + " WHERE MPL_ID = '" + batchCd.ToString() + "' "
                        + " AND MPL_PROCD = '" + prodCd.ToString() + "' "
                        + " AND MPL_TYPE = 'UMT' ");
                    if (dtMeatQty.Rows.Count > 0 && dtMeatQty.Rows[0]["TOT_QTY"].ToString() != "")
                    {
                        lblMeatQty = Convert.ToDouble(dtMeatQty.Rows[0]["TOT_QTY"].ToString()).ToString("n3");
                    }

                    DataTable dtIngredientsQty = dbHlpr.FetchData("SELECT "
                        + " SUM(MPL_QTY) AS TOT_QTY "
                        + " FROM PRC_HST_MRNLN "
                        + " WHERE MPL_ID = '" + batchCd.ToString() + "' "
                        + " AND MPL_PROCD = '" + prodCd.ToString() + "' "
                        + " AND MPL_TYPE = 'ING' ");
                    if (dtIngredientsQty.Rows.Count > 0 && dtIngredientsQty.Rows[0]["TOT_QTY"].ToString() != "")
                    {
                        lblIngrdntsQty = Convert.ToDouble(dtIngredientsQty.Rows[0]["TOT_QTY"].ToString()).ToString("n3");
                    }

                    double totalQty = Convert.ToDouble(lblMeatQty) + Convert.ToDouble(lblIngrdntsQty);
                    lblTotalQty = totalQty.ToString("n3");
                }
            }

            return Json(new
            {
                success = true,
                totMeatQty = lblMeatQty,
                totIngQty = lblIngrdntsQty,
                totQty = lblTotalQty,
            });
        }

        [HttpPost]
        public ActionResult GrvDropDown(string code)
        {
            DataTable dtRmt = RawMaterial.GetByCode(code);

            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            DataTable dtGrvs = new DataTable();
            if (dtRmt.Rows.Count > 0)
            {
                dtGrvs = dbHlpr.FetchData("SELECT "
                        + " RCV_FMSGRV, RCV_RMCOD "
                        + " FROM INV_GRV_RCVNG "
                        + " WHERE RCV_RMCOD = '" + dtRmt.Rows[0]["RMT_LNKCD"].ToString() + "' "
                        + " ORDER BY RCV_FMSGRV DESC ");
            }

            return Json(new { success = true, data = JsonConvert.SerializeObject(dtGrvs) });
        }

        [NonAction]
        private KeyValuePair<bool, string> ValidateProcessData(FormCollection f)
        {
            string msg = "";
            if (f["prod_cd"] == null || f["prod_cd"].ToString().Length <= 0)
            {
                msg = "Product is required";
                return new KeyValuePair<bool, string>(false, msg);
            }

            DateTime startdate;
            if (f["bch_start_dt"] == null || !DateTime.TryParse(f["bch_start_dt"].ToString(), out startdate))
            {
                msg = "Invalid Start Date";
                return new KeyValuePair<bool, string>(false, msg);
            }

            if (f["prcs_id"] == "-" || f["prcs_id"].Trim().Length < 5)
            {
                msg = "Invalid Process ID";
                return new KeyValuePair<bool, string>(false, msg);
            }

            string[] MSC_ITMS = f["msc_itmcds[]"] == null ? new string[] { } : f["msc_itmcds[]"].Split(',');
            string[] MSC_GRVS = f["msc_grvs[]"] == null ? new string[] { } : f["msc_grvs[]"].Split(',');
            string[] MSC_PROCS = f["msc_procsids[]"] == null ? new string[] { } : f["msc_procsids[]"].Split(',');
            string[] MSC_QTYS = f["msc_qtys[]"] == null ? new string[] { } : f["msc_qtys[]"].Split(',');
            if (MSC_ITMS.Length <= 0 || MSC_PROCS.Length <= 0 || MSC_QTYS.Length <= 0 || MSC_PROCS.Length != MSC_QTYS.Length)
            {
                msg = "Item details are required. Please enter GRV and Quantity";
                return new KeyValuePair<bool, string>(false, msg);
            }
            string meatGridErrors = "";
            for (int i = 0; i < MSC_ITMS.Length; i++)
            {
                if (MSC_GRVS[i].ToString().Length <= 0 && MSC_PROCS[i].ToString().Length <= 0)
                {
                    meatGridErrors += "Meat GRV # or Process ID is required for all rows.<br/>";
                }
                if (MSC_QTYS[i].ToString().Length <= 0)
                {
                    meatGridErrors += "Meat Quantity is required for all rows<br/>";
                }
            }
            if (meatGridErrors.Trim().Length > 0)
            {
                msg = meatGridErrors;
                return new KeyValuePair<bool, string>(false, msg);
            }

            string miscGridErrors = "";
            for (int i = 0; i < MSC_ITMS.Length; i++)
            {
                if (MSC_QTYS[i].ToString().Length <= 0)
                {
                    miscGridErrors += "Misc. Quantity is required for all rows<br/>";
                }
            }
            if (miscGridErrors.Trim().Length > 0)
            {
                msg = miscGridErrors;
                return new KeyValuePair<bool, string>(false, msg);
            }

            return new KeyValuePair<bool, string>(true, "");
        }

        [NonAction]
        private KeyValuePair<bool, string> ValidateMarinatedQtys(FormCollection f)
        {
            string msg = "";
            string[] MSC_ITMS = f["msc_itmcds[]"] == null ? new string[] { } : f["msc_itmcds[]"].Split(',');
            string[] MSC_QTYS = f["msc_qtys[]"] == null ? new string[] { } : f["msc_qtys[]"].Split(',');

            double totalPreparedQty = 0.000;

            for (int i = 0; i < MSC_ITMS.Length; i++)
            {
                if (MSC_ITMS[i].ToString() == "MS-9972" || MSC_ITMS[i].ToString() == "MS-9967")
                {
                    if (MSC_QTYS[i].ToString().Length > 0)
                    {
                        totalPreparedQty += Convert.ToDouble(MSC_QTYS[i].ToString());
                    }
                }

            }

            if (Math.Round(totalPreparedQty, 3, MidpointRounding.AwayFromZero) > Math.Round(Convert.ToDouble(f["totQtyHdn"]), 3, MidpointRounding.AwayFromZero))
            {
                msg = "Total Prepared weight should be less than Marinated Qty";
                return new KeyValuePair<bool, string>(false, msg);
            }

            return new KeyValuePair<bool, string>(true, "");
        }

        [HttpPost]
        public ActionResult SaveProcess(FormCollection form)
        {
            KeyValuePair<bool, string> validateData = this.ValidateProcessData(form);
            if (validateData.Key)
            {
                DateTime dtNow = DateTime.Now;

                if (form["prod_cd"] == null || form["prod_cd"].ToString().Trim().Length <= 0)
                {
                    return Json(new
                    {
                        success = false,
                        msg = "Error :: Please select Item to save."
                    });
                }

                string[] MSC_ITMS = form["msc_itmcds[]"] == null ? new string[] { } : form["msc_itmcds[]"].Split(',');
                string[] MSC_GRVS = form["msc_grvs[]"] == null ? new string[] { } : form["msc_grvs[]"].Split(',');
                string[] MSC_PROCS = form["msc_procsids[]"] == null ? new string[] { } : form["msc_procsids[]"].Split(',');
                string[] MSC_QTYS = form["msc_qtys[]"] == null ? new string[] { } : form["msc_qtys[]"].Split(',');

                double totalWeightIn = 0;
                for (int i = 0; i < MSC_ITMS.Length; i++)
                {
                    double qty;
                    if (double.TryParse(MSC_QTYS[i].ToString(), out qty))
                    {
                        totalWeightIn = totalWeightIn + qty;
                    }
                }
                if (totalWeightIn <= 0)
                {
                    return Json(new
                    {
                        success = false,
                        msg = "Item total quantity must be greater than Zero."
                    });
                }

                KeyValuePair<bool, string> validateMarinationData = ValidateMarinatedQtys(form);
                if (!validateMarinationData.Key)
                {
                    return Json(new
                    {
                        success = false,
                        msg = validateMarinationData.Value
                    });
                }

                string ProdCd = form["prod_cd"];
                string ProdDesc = Product.GetByCode(ProdCd).Rows[0]["PRO_DESC1"].ToString().Trim();
                string RefNo = form["ref_no"].Trim();
                DateTime deBatchDate = Convert.ToDateTime(form["bch_start_dt"]);
                string StartDate = Convert.ToDateTime(form["bch_start_dt"]).ToString("yyyy-MM-dd");
                string StartTime = "00:00:00"; // Convert.ToDateTime(form["bch_start_tm"]).ToString("HH:mm:ss");
                string EndDate = Convert.ToDateTime(form["bch_end_dt"]).ToString("yyyy-MM-dd");
                string EndTime = "00:00:00"; // Convert.ToDateTime(form["bch_end_tm"]).ToString("HH:mm:ss");
                string weekToPut = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(new DateTime(deBatchDate.Year, deBatchDate.Month, deBatchDate.Day), CalendarWeekRule.FirstDay, DayOfWeek.Saturday).ToString();
                string lblProcessId = form["prcs_id"];

                DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
                dbHlpr.ExecuteNonQuery("DELETE FROM PRC_TRX_SKWHD WHERE SPH_ID = '" + lblProcessId + "'");
                dbHlpr.ExecuteNonQuery("DELETE FROM PRC_TRX_SKWLN WHERE SPL_ID = '" + lblProcessId + "'");

                string bchCode = lblProcessId;
                string bchNo = lblProcessId.Substring(lblProcessId.IndexOf('-') + 1);

                string hdrQry = "INSERT INTO PRC_TRX_SKWHD ( "
                    + " SPH_ID, SPH_BCHNO, SPH_PROCD, SPH_DESC1, "
                    + " SPH_SDATE, SPH_STIME, SPH_EDATE, SPH_ETIME, "
                    + " SPH_WEEK, SPH_WTKGS, SPH_REFNO, SPH_ENTBY, SPH_ENTDT, "
                    + " SPH_ENTIM "
                    + " ) VALUES ( "
                    + " '" + bchCode + "', '" + bchNo + "', '" + ProdCd + "', '" + ProdDesc + "', "
                    + " '" + StartDate + "', '" + StartTime + "', '" + EndDate + "', '" + EndTime + "', "
                    + " '" + weekToPut + "', '" + totalWeightIn + "', '" + RefNo + "', '" + Session["UserId"] + "', '" + dtNow + "', '" + dtNow + "' "
                    + " ) ";
                dbHlpr.ExecuteNonQuery(hdrQry);

                // Miscellaneous
                for (int i = 0; i < MSC_ITMS.Length; i++)
                {
                    string itmCode = MSC_ITMS[i].ToString();
                    string itmDesc = "";
                    DataTable dtItmDetails = RawMaterial.GetByCode(MSC_ITMS[i]);
                    if (dtItmDetails.Rows.Count > 0)
                    {
                        itmDesc = dtItmDetails.Rows[0]["RMT_DESC1"].ToString();
                    }
                    else
                    {
                        dtItmDetails = BomLegend.GetByCode(MSC_ITMS[i]);
                        itmDesc = dtItmDetails.Rows[0]["BLG_NAME"].ToString();
                    }

                    string itmGrv = MSC_GRVS[i].ToString();
                    string itmProcs = MSC_PROCS[i].ToString();
                    string itmQty = MSC_QTYS[i].ToString();

                    string itmType = "MSC";
                    if (itmCode.IndexOf("MS-") != 0)
                    {
                        itmType = "RMT";
                    }

                    string wsLinQry = "INSERT INTO PRC_TRX_SKWLN ("
                        + " SPL_ID, SPL_BCHNO, "
                        + " SPL_SDATE, SPL_STIME, SPL_EDATE, SPL_ETIME, "
                        + " SPL_WEEK, SPL_PROCD, SPL_WTKGS, "
                        + " SPL_ITMCD, SPL_DESC1, SPL_TYPE, SPL_GRVNO, SPL_PRCID, SPL_QTY, "
                        + " SPL_ENTBY, SPL_ENTDT, SPL_ENTIM "
                        + " ) VALUES ( "
                        + " '" + bchCode + "', '" + bchNo + "', "
                        + " '" + StartDate + "', '" + StartTime + "', '" + EndDate + "', '" + EndTime + "', "
                        + " '" + weekToPut + "', '" + ProdCd + "', '" + totalWeightIn + "', "
                        + " '" + itmCode + "', '" + itmDesc + "', '" + itmType + "', '" + itmGrv + "', '" + itmProcs + "', '" + itmQty + "', "
                        + " '" + Session["UserId"] + "', '" + dtNow + "', '" + dtNow + "' "
                        + " ) ";
                    dbHlpr.ExecuteNonQuery(wsLinQry);
                }

                SetSuccessMessage("Process Eneterd Successfully");

                return Json(new { success = true, url = Url.Action("ProcessInquiry", "Skewering") });
            }

            return Json(new { success = false, msg = validateData.Value });
        }

        [HttpPost]
        public ActionResult PostProcess(FormCollection form)
        {
            KeyValuePair<bool, string> validateData = this.ValidateProcessData(form);
            if (!validateData.Key || form["posting"] != "true")
            {
                return Json(new { success = false, msg = validateData.Value });
            }

            if (form["prod_cd"] == null || form["prod_cd"].ToString().Trim().Equals(""))
            {
                return Json(new { success = false, msg = "Error :: Please select Item to Post." });
            }

            DateTime dateTimeNow = DateTime.Now;
            string dateTimeNowDate = dateTimeNow.ToString("yyyy-MM-dd");
            string dateTimeNowTime = dateTimeNow.ToString("HH:mm:ss.fff");
            
            string ProdCd = form["prod_cd"];
            string lblProcessId = form["prcs_id"];

            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            dbHlpr.ExecuteScalarQuery("INSERT INTO PRC_HST_SKWHD ( "
                + " SPH_ID, SPH_BCHNO, SPH_PROCD, "
                + " SPH_DESC1, SPH_SDATE, SPH_STIME, "
                + " SPH_EDATE, SPH_ETIME, SPH_WEEK, "
                + " SPH_WTKGS, SPH_REFNO, SPH_ENTBY, SPH_ENTDT, "
                + " SPH_ENTIM, SPH_UPDBY, SPH_LSUPD, SPH_LSUPT, "
                + " SPH_PSTBY, SPH_PSTDT, SPH_PSTIM "
                + " ) SELECT "
                + " SPH_ID, SPH_BCHNO, SPH_PROCD, "
                + " SPH_DESC1, SPH_SDATE, SPH_STIME, "
                + " SPH_EDATE, SPH_ETIME, SPH_WEEK, "
                + " SPH_WTKGS, SPH_REFNO, SPH_ENTBY, SPH_ENTDT, "
                + " SPH_ENTIM, SPH_UPDBY, SPH_LSUPD, SPH_LSUPT, "
                + " '" + Session["UserId"] + "', '" + dateTimeNowDate + "', '" + dateTimeNowTime + "' "
                + " FROM PRC_TRX_SKWHD "
                + " WHERE PRC_TRX_SKWHD.SPH_ID = '" + lblProcessId.Trim() + "'");

            dbHlpr.ExecuteScalarQuery("INSERT INTO PRC_HST_SKWLN ( "
                + " SPL_ID, SPL_BCHNO, SPL_SDATE, "
                + " SPL_STIME, SPL_EDATE, SPL_ETIME, "
                + " SPL_WEEK, SPL_PROCD, SPL_WTKGS, "
                + " SPL_ITMCD, SPL_DESC1, SPL_TYPE, "
                + " SPL_GRVNO, SPL_PRCID, SPL_QTY, SPL_ENTBY, "
                + " SPL_ENTDT, SPL_ENTIM, SPL_UPDBY, "
                + " SPL_LSUPD, SPL_LSUPT, "
                + " SPL_PSTBY, SPL_PSTDT, SPL_PSTIM "
                + " ) SELECT "
                + " SPL_ID, SPL_BCHNO, SPL_SDATE, "
                + " SPL_STIME, SPL_EDATE, SPL_ETIME, "
                + " SPL_WEEK, SPL_PROCD, SPL_WTKGS, "
                + " SPL_ITMCD, SPL_DESC1, SPL_TYPE, "
                + " SPL_GRVNO, SPL_PRCID, SPL_QTY, SPL_ENTBY, "
                + " SPL_ENTDT, SPL_ENTIM, SPL_UPDBY, "
                + " SPL_LSUPD, SPL_LSUPT, "
                + " '" + Session["UserId"] + "', '" + dateTimeNowDate + "', '" + dateTimeNowTime + "' "
                + " FROM PRC_TRX_SKWLN "
                + " WHERE PRC_TRX_SKWLN.SPL_ID = '" + lblProcessId.Trim() + "'");

            dbHlpr.ExecuteScalarQuery("DELETE FROM PRC_TRX_SKWHD WHERE SPH_ID = '" + lblProcessId.Trim() + "' ");
            dbHlpr.ExecuteScalarQuery("DELETE FROM PRC_TRX_SKWLN WHERE SPL_ID = '" + lblProcessId.Trim() + "' ");

            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            string comments = "Posted MCPU Skewering Process Batch for "
                + ProdCd.ToString() + " and Batch : '" + lblProcessId.Trim() + "' into "
                + controllerName + "/" + actionName + " form at " + System.DateTime.Now.ToString();
            string extras = "PRCS of Prod Code " + ProdCd.ToString() + " & '" + lblProcessId.Trim() + "' Posted";
            dbHlpr.InsertUserLog(Session["UserId"].ToString(), Session["UserName"].ToString(), DateTime.Now, comments, extras);

            SetSuccessMessage("Skewering Posted Successfully.");

            return Json(new { success = true, url = Url.Action("ProcessInquiry") });
        }
    }
}