﻿using MCPU.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MCPU.Controllers.Admin.MCPU
{
    public class YieldingController : AuthController
    {
        string BOM_VIEW = "~/Views/Admin/MCPU/Yielding/BoM.cshtml";
        string PROCESS_VIEW = "~/Views/Admin/MCPU/Yielding/Process.cshtml";
        string PROCESSINQUIRY_VIEW = "~/Views/Admin/MCPU/Yielding/ProcessInquiry.cshtml";
        public ActionResult BoM()
        {
            DataTable dt = RawMaterial.GetAll();

            ViewBag.RMTS = new SelectList(dt.AsDataView(), "RMT_CODE", "RMT_CODE");

            return View(BOM_VIEW);
        }

        public ActionResult ProcessInquiry()
        {
            DataTable dt = RawMaterial.GetAll();

            ViewBag.RMTS = new SelectList(dt.AsDataView(), "RMT_CODE", "RMT_CODE");

            return View(PROCESSINQUIRY_VIEW);
        }

        public ActionResult Process(string rm, string pr)
        {
            DataTable dt = RawMaterial.GetAll();
            ViewBag.RMTS = new SelectList(dt.AsDataView(), "RMT_CODE", "RMT_CODE");
            ViewBag.UMTS = new SelectList(dt.AsDataView(), "RMT_CODE", "RMT_CODE");

            ViewBag.BatchStartDateEnabled = true;
            ViewBag.BatchStartDate = "";
            ViewBag.BatchEndDate = "";
            ViewBag.BatchStartTime = "";
            ViewBag.BatchEndTime = "";
            ViewBag.ProcessId = "";
            ViewBag.RmCodeReadOnly = false;
            ViewBag.RmCode = "";
            ViewBag.RefNo = "";
            ViewBag.PostEnabled = false;
            if (rm != null && pr != null && rm != "" && pr != "")
            {

                DataTable YieldingDetails = YieldingModel.GetProcessHeader(rm, pr);
                if (YieldingDetails.Rows.Count > 0)
                {
                    ViewBag.RMTS = new SelectList(dt.AsDataView(), "RMT_CODE", "RMT_CODE", rm);
                    ViewBag.RmCode = rm;
                    ViewBag.ProcessId = pr;
                    ViewBag.PostEnabled = true;
                    ViewBag.BatchStartDateEnabled = false;

                    try
                    {
                        ViewBag.BatchStartDate = Convert.ToDateTime(YieldingDetails.Rows[0]["PRH_SDATE"].ToString()).ToString("dd-MMM-yyyy");
                    }
                    catch (Exception ex)
                    {
                        ViewBag.BatchStartDate = "";
                    }

                    try
                    {
                        ViewBag.BatchEndDate = Convert.ToDateTime(YieldingDetails.Rows[0]["PRH_EDATE"].ToString()).ToString("dd-MMM-yyyy");
                    }
                    catch (Exception ex)
                    {
                        ViewBag.BatchEndDate = "";
                    }

                    try
                    {
                        ViewBag.BatchStartTime = Convert.ToDateTime(YieldingDetails.Rows[0]["PRH_STIME"].ToString()).ToString("HH:mm");
                    }
                    catch (Exception ex)
                    {
                        ViewBag.BatchStartTime = "00:00";
                    }

                    try
                    {
                        ViewBag.BatchEndTime = Convert.ToDateTime(YieldingDetails.Rows[0]["PRH_ETIME"].ToString()).ToString("HH:mm");
                    }
                    catch (Exception ex)
                    {
                        ViewBag.BatchEndTime = "00:00";
                    }

                    ViewBag.RmCodeReadOnly = true;
                    ViewBag.RefNo = YieldingDetails.Rows[0]["PRH_REFNO"].ToString();
                }
            }

            return View(PROCESS_VIEW);
        }

        [HttpPost]
        public ActionResult LoadProcess(string code, string prcs, string sndr, bool rmRdOnly)
        {
            if (sndr.ToLower() == "rmt")
            {
                DataTable DT_GRVS = new DataTable();
                DataTable DT_RAWMEAT = new DataTable();
                DataTable DT_USABLES = new DataTable();
                DataTable DT_WASTAGES = new DataTable();

                string desc1 = "";
                string desc2 = "";
                string uom = "";

                string processId = "";
                try
                {
                    processId = YieldingModel.GetNextProcessId(code, Convert.ToDateTime(prcs));
                }
                catch (Exception)
                {
                    processId = prcs;
                }

                if (code != null)
                {
                    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
                    DataTable dt = dbHlpr.FetchData("SELECT "
                        + " RMT_CODE, RMT_DESC1, RMT_DESC2, RMT_TRKMD, RMT_CATG, RMT_SCATG, "
                        + " RMT_UDF1, RMT_UDF2, RMT_UDF3, RMT_UDF4, RMT_UOM, RMT_WT, "
                        + " RMT_AVCST, RMT_ALCOD, RMT_STATS, RMT_LNKCD "
                        + " FROM INV_MSTR_RWMAT "
                        + " WHERE RMT_CODE = '" + code + "' ");
                    if (dt.Rows.Count > 0)
                    {
                        desc1 = dt.Rows[0]["RMT_DESC1"].ToString();
                        desc2 = dt.Rows[0]["RMT_DESC2"].ToString();
                        uom = dt.Rows[0]["RMT_UOM"].ToString();

                        DT_GRVS = dbHlpr.FetchData("SELECT "
                                + " RCE_KEY, RCE_CPGRV, RCE_RMCOD, RCE_EXPDT "
                                + " FROM INV_GRV_EXPIRY "
                                + " WHERE RCE_RMCOD = '" + dt.Rows[0]["RMT_LNKCD"].ToString() + "' "
                                + " ORDER BY RCE_CPGRV, RCE_EXPDT DESC ");

                        DT_RAWMEAT = dbHlpr.FetchData("SELECT "
                            + " RCE_KEY AS ITMGRV, PRL_QTY AS ITMQTY, "
                            + " PRL_EXPDT AS ITMEXPRY, RCV_REFNO AS ITMREFNO "
                            + " FROM PRC_TRX_YLDLN "
                            + " LEFT JOIN INV_GRV_RCVNG ON RCV_RMCOD = '" + code + "' AND PRL_GRVNO = RCV_CPGRV "
                            + " LEFT JOIN INV_GRV_EXPIRY ON RCE_RMCOD = '" + code + "' AND PRL_GRVNO = RCE_CPGRV AND PRL_EXPDT = RCE_EXPDT "
                            + " WHERE PRL_ID = '" + processId + "' AND PRL_TYPE = 'RMT' ");
                        if (rmRdOnly)
                        {
                            DT_USABLES = dbHlpr.FetchData("SELECT "
                                + " PRL_ITMCD AS ITMCOD, RMT_DESC1 AS ITMDESC1, COALESCE(PRL_QTY, 0.000) AS ITMQTY "
                                + " FROM PRC_TRX_YLDLN "
                                + " LEFT JOIN BOM_MST_YLDNG ON PRL_ITMCD = YBM_ITMNO AND PRL_RMCOD = YBM_RMCOD "
                                + " LEFT JOIN INV_MSTR_RWMAT ON PRL_ITMCD = RMT_CODE "
                                + " WHERE PRL_TYPE = 'UMT' "
                                + " AND PRL_RMCOD = '" + code + "' AND PRL_ID = '" + processId + "' "
                                + " ORDER BY YBM_SEQNO ");
                            DT_WASTAGES = dbHlpr.FetchData("SELECT "
                                + " PRL_ITMCD AS ITMCOD, RMT_DESC1 AS ITMDESC1, COALESCE(PRL_QTY, 0.000) AS ITMQTY "
                                + " FROM PRC_TRX_YLDLN "
                                + " LEFT JOIN BOM_MST_YLDNG ON PRL_ITMCD = YBM_ITMNO AND PRL_RMCOD = YBM_RMCOD "
                                + " LEFT JOIN INV_MSTR_RWMAT ON PRL_ITMCD = RMT_CODE "
                                + " WHERE PRL_TYPE = 'WST' "
                                + " AND PRL_RMCOD = '" + code + "' AND PRL_ID = '" + processId + "' "
                                + " ORDER BY YBM_SEQNO ");
                        }
                        else
                        {
                            DT_USABLES = dbHlpr.FetchData("SELECT "
                                + " YBM_ITMNO AS ITMCOD, RMT_DESC1 AS ITMDESC1, COALESCE(PRL_QTY, 0.000) AS ITMQTY "
                                + " FROM BOM_MST_YLDNG "
                                + " LEFT JOIN INV_MSTR_RWMAT ON YBM_ITMNO = RMT_CODE "
                                + " LEFT JOIN PRC_TRX_YLDLN ON YBM_ITMNO = PRL_ITMCD AND PRL_TYPE = 'UMT' AND PRL_ID = '" + processId + "' "
                                + " WHERE YBM_ITFLG = '2' "
                                + " AND YBM_RMCOD = '" + code + "' "
                                + " ORDER BY YBM_SEQNO ");
                            DT_WASTAGES = dbHlpr.FetchData("SELECT "
                                + " YBM_ITMNO AS ITMCOD, RMT_DESC1 AS ITMDESC1, COALESCE(PRL_QTY, 0.000) AS ITMQTY "
                                + " FROM BOM_MST_YLDNG "
                                + " LEFT JOIN INV_MSTR_RWMAT ON YBM_ITMNO = RMT_CODE "
                                + " LEFT JOIN PRC_TRX_YLDLN ON YBM_ITMNO = PRL_ITMCD AND PRL_TYPE = 'WST' AND PRL_ID = '" + processId + "' "
                                + " WHERE YBM_ITFLG = '3' "
                                + " AND YBM_RMCOD = '" + code + "' "
                                + " ORDER BY YBM_SEQNO ");
                        }

                        return Json(new
                        {
                            success = true,
                            rmData = new { rmCode = code, rmDesc1 = desc1, rmDesc2 = desc2, rmUom = uom },
                            grvsData = JsonConvert.SerializeObject(DT_GRVS),
                            rmTblData = JsonConvert.SerializeObject(DT_RAWMEAT),
                            usTblData = JsonConvert.SerializeObject(DT_USABLES),
                            wsTblData = JsonConvert.SerializeObject(DT_WASTAGES),
                            prcsId = processId
                        });
                    }
                    else
                    {
                        return Json(new
                        {
                            success = false,
                            msg = "Error :: Item not found."
                        });
                    }
                } // end if RMT code is null or empty
                else
                {
                    return Json(new
                    {
                        success = false,
                        msg = "Error :: Please select a Raw Meat item to save."
                    });
                } // end if RMT code is null or empty
            } // end if sender is RMT
            else if (sndr.ToLower() == "sdate")
            {
                string processId = "";
                try
                {
                    processId = YieldingModel.GetNextProcessId(code, Convert.ToDateTime(prcs));
                }
                catch (Exception)
                {
                    processId = "";
                }

                return Json(new
                {
                    success = true,
                    prcsId = processId
                });
            }

            return Json(new
            {
                success = false,
                msg = "Error :: Invalid Request."
            });
        }

        [HttpPost]
        public ActionResult PostProcess(FormCollection form)
        {
            KeyValuePair<bool, string> validateData = this.ValidateProcessData(form);
            if (validateData.Key && form["posting"] == "true")
            {
                string[] RMT_GRVS = form["rm_grvs[]"].Split(',');
                string[] RMT_QTYS = form["rm_qtys[]"].Split(',');

                string[] UMT_ITMS = form["us_itmcds[]"] == null ? new string[] { } : form["us_itmcds[]"].Split(',');
                string[] UMT_QTYS = form["us_qtys[]"] == null ? new string[] { } : form["us_qtys[]"].Split(',');
                string[] WST_ITMS = form["ws_itmcds[]"] == null ? new string[] { } : form["ws_itmcds[]"].Split(',');
                string[] WST_QTYS = form["ws_qtys[]"] == null ? new string[] { } : form["ws_qtys[]"].Split(',');

                double totalWeightIn = 0;
                for (int i = 0; i < RMT_GRVS.Length; i++)
                {
                    double qty;
                    if (double.TryParse(RMT_QTYS[i].ToString(), out qty))
                    {
                        totalWeightIn = totalWeightIn + qty;
                    }
                }
                if (totalWeightIn <= 0)
                {
                    return Json(new { success = false, msg = "Raw material quantity must be greater than Zero." });
                }

                double totalWeightOut = 0;
                for (int i = 0; i < UMT_ITMS.Length; i++)
                {
                    double qty;
                    if (double.TryParse(UMT_QTYS[i].ToString(), out qty))
                    {
                        totalWeightOut = totalWeightOut + qty;
                    }
                }
                for (int i = 0; i < WST_ITMS.Length; i++)
                {
                    double qty;
                    if (double.TryParse(WST_QTYS[i].ToString(), out qty))
                    {
                        totalWeightOut = totalWeightOut + qty;
                    }
                }
                if (Math.Round(totalWeightIn, 3, MidpointRounding.AwayFromZero) != Math.Round(totalWeightOut, 3, MidpointRounding.AwayFromZero))
                {
                    return Json(new { success = false, msg = "Posting is not allowed with difference of \"" + (totalWeightIn - totalWeightOut).ToString("n3") + "\" in Raw Meat Qty and total Yield Weight." });
                }

                string RmCd = form["rmt_cd"];
                string lblProcessId = form["prcs_id"];

                DateTime dateTimeNow = DateTime.Now;
                string dateTimeNowDate = dateTimeNow.ToString("yyyy-MM-dd");
                string dateTimeNowTime = dateTimeNow.ToString("HH:mm:ss.fff");

                DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
                dbHlpr.ExecuteScalarQuery("INSERT INTO PRC_HST_YLDHD ( "
                    + " PRH_ID, PRH_BCHNO, PRH_RMCOD, "
                    + " PRH_DESC1, PRH_SDATE, PRH_STIME, "
                    + " PRH_EDATE, PRH_ETIME, PRH_WEEK, "
                    + " PRH_WTKGS, PRH_REFNO, PRH_ENTBY, PRH_ENTDT, "
                    + " PRH_ENTIM, PRH_UPDBY, PRH_LSUPD, PRH_LSUPT, "
                    + " PRH_PSTBY, PRH_PSTDT, PRH_PSTIM "
                    + " ) SELECT "
                    + " PRH_ID, PRH_BCHNO, PRH_RMCOD, "
                    + " PRH_DESC1, PRH_SDATE, PRH_STIME, "
                    + " PRH_EDATE, PRH_ETIME, PRH_WEEK, "
                    + " PRH_WTKGS, PRH_REFNO, PRH_ENTBY, PRH_ENTDT, "
                    + " PRH_ENTIM, PRH_UPDBY, PRH_LSUPD, PRH_LSUPT, "
                    + " '" + Session["UserId"] + "', '" + dateTimeNowDate + "', '" + dateTimeNowTime + "' "
                    + " FROM PRC_TRX_YLDHD "
                    + " WHERE PRC_TRX_YLDHD.PRH_ID = '" + lblProcessId.Trim() + "'");

                dbHlpr.ExecuteScalarQuery("INSERT INTO PRC_HST_YLDLN ( "
                    + " PRL_ID, PRL_BCHNO, PRL_SDATE, "
                    + " PRL_STIME, PRL_EDATE, PRL_ETIME, "
                    + " PRL_WEEK, PRL_RMCOD, PRL_WTKGS, "
                    + " PRL_ITMCD, PRL_DESC1, PRL_TYPE, "
                    + " PRL_GRVNO, PRL_EXPDT, PRL_QTY, PRL_ENTBY, "
                    + " PRL_ENTDT, PRL_ENTIM, PRL_UPDBY, "
                    + " PRL_LSUPD, PRL_LSUPT, "
                    + " PRL_PSTBY, PRL_PSTDT, PRL_PSTIM "
                    + " ) SELECT "
                    + " PRL_ID, PRL_BCHNO, PRL_SDATE, "
                    + " PRL_STIME, PRL_EDATE, PRL_ETIME, "
                    + " PRL_WEEK, PRL_RMCOD, PRL_WTKGS, "
                    + " PRL_ITMCD, PRL_DESC1, PRL_TYPE, "
                    + " PRL_GRVNO, PRL_EXPDT, PRL_QTY, PRL_ENTBY, "
                    + " PRL_ENTDT, PRL_ENTIM, PRL_UPDBY, "
                    + " PRL_LSUPD, PRL_LSUPT, "
                    + " '" + Session["UserId"] + "', '" + dateTimeNowDate + "', '" + dateTimeNowTime + "' "
                    + " FROM PRC_TRX_YLDLN "
                    + " WHERE PRC_TRX_YLDLN.PRL_ID = '" + lblProcessId.Trim() + "'");

                dbHlpr.ExecuteScalarQuery("DELETE FROM PRC_TRX_YLDHD WHERE PRH_ID = '" + lblProcessId.Trim() + "' ");
                dbHlpr.ExecuteScalarQuery("DELETE FROM PRC_TRX_YLDLN WHERE PRL_ID = '" + lblProcessId.Trim() + "' ");

                string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
                string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                string comments = "Posted MCPU Yielding Process Batch for "
                    + RmCd + " and Batch : '" + lblProcessId.Trim() + "' into "
                    + controllerName + "/" + actionName + " form at " + System.DateTime.Now.ToString();
                string extras = "PRCS of RM Code " + RmCd + " & '" + lblProcessId.Trim() + "' Posted";
                dbHlpr.InsertUserLog(Session["UserId"].ToString(), Session["UserName"].ToString(), DateTime.Now, comments, extras);

                SetSuccessMessage("Yielding Process Posted Successfully.");

                return Json(new { success = true, url = Url.Action("ProcessInquiry") });
            }

            return Json(new { success = false, msg = validateData.Value });
        }

        [HttpPost]
        public ActionResult SaveProcess(FormCollection form)
        {
            KeyValuePair<bool, string> validateData = this.ValidateProcessData(form);
            if (validateData.Key)
            {
                DateTime dtNow = DateTime.Now;

                string[] RMT_GRVS = form["rm_grvs[]"].Split(',');
                string[] RMT_QTYS = form["rm_qtys[]"].Split(',');

                string[] UMT_ITMS = form["us_itmcds[]"].Split(',');
                string[] UMT_QTYS = form["us_qtys[]"].Split(',');
                string[] WST_ITMS = form["ws_itmcds[]"].Split(',');
                string[] WST_QTYS = form["ws_qtys[]"].Split(',');

                double totalWeightIn = 0;
                for (int i = 0; i < RMT_QTYS.Length; i++)
                {
                    double qty;
                    if (double.TryParse(RMT_QTYS[i].ToString(), out qty))
                    {
                        totalWeightIn = totalWeightIn + qty;
                    }
                }
                if (totalWeightIn <= 0)
                {
                    return Json(new { success = false, msg = "Raw Material GRV details required" });
                }

                double totalWeightOut = 0;
                for (int i = 0; i < UMT_QTYS.Length; i++)
                {
                    double qty;
                    if (double.TryParse(UMT_QTYS[i].ToString(), out qty))
                    {
                        totalWeightOut = totalWeightOut + qty;
                    }
                }
                for (int i = 0; i < WST_QTYS.Length; i++)
                {
                    double qty;
                    if (double.TryParse(WST_QTYS[i].ToString(), out qty))
                    {
                        totalWeightOut = totalWeightOut + qty;
                    }
                }
                if (Math.Round(totalWeightIn, 3, MidpointRounding.AwayFromZero) != Math.Round(totalWeightOut, 3, MidpointRounding.AwayFromZero))
                {
                    if (form["confirmed"] != "CONFIRMED")
                    {
                        return Json(new
                        {
                            success = false,
                            confirmationmsg = "Variance in Quantity<br/>Difference of \"" + (totalWeightIn - totalWeightOut).ToString("n3") + "\" in Raw Meat Qty and total Yield Weight. Do you want to Continue?"
                        });
                    }
                }


                string RmCd = form["rmt_cd"];
                string RmDesc1 = RawMaterial.GetByCode(RmCd).Rows[0]["RMT_DESC1"].ToString();
                string RefNo = form["ref_no"].Trim();
                DateTime deBatchDate = Convert.ToDateTime(form["bch_start_dt"]);
                string StartDate = Convert.ToDateTime(form["bch_start_dt"]).ToString("yyyy-MM-dd");
                string StartTime = "00:00:00"; // Convert.ToDateTime(form["bch_start_tm"]).ToString("HH:mm:ss");
                string EndDate = Convert.ToDateTime(form["bch_end_dt"]).ToString("yyyy-MM-dd");
                string EndTime = "00:00:00"; // Convert.ToDateTime(form["bch_end_tm"]).ToString("HH:mm:ss");
                string weekToPut = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(new DateTime(deBatchDate.Year, deBatchDate.Month, deBatchDate.Day), CalendarWeekRule.FirstDay, DayOfWeek.Saturday).ToString();
                string lblProcessId = form["prcs_id"];

                DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
                dbHlpr.ExecuteNonQuery("DELETE FROM PRC_TRX_YLDHD WHERE PRH_ID = '" + lblProcessId + "'");
                dbHlpr.ExecuteNonQuery("DELETE FROM PRC_TRX_YLDLN WHERE PRL_ID = '" + lblProcessId + "'");

                string hdrQry = "INSERT INTO PRC_TRX_YLDHD ( "
                    + " PRH_ID, PRH_BCHNO, PRH_RMCOD, PRH_DESC1, "
                    + " PRH_SDATE, PRH_STIME, PRH_EDATE, PRH_ETIME, "
                    + " PRH_WEEK, PRH_WTKGS, PRH_REFNO, PRH_ENTBY, PRH_ENTDT, "
                    + " PRH_ENTIM "
                    + " ) VALUES ( "
                    + " '" + lblProcessId + "', '" + lblProcessId.Substring(lblProcessId.IndexOf('-') + 1) + "', '" + RmCd + "', '" + RmDesc1 + "', "
                    + " '" + StartDate + "', '" + StartTime + "', '" + EndDate + "', '" + EndTime + "', "
                    + " '" + weekToPut + "', '" + totalWeightIn + "', '" + RefNo + "', '" + Session["UserId"] + "', '" + dtNow + "', '" + dtNow + "' "
                    + " ) ";
                dbHlpr.ExecuteNonQuery(hdrQry);

                for (int i = 0; i < RMT_GRVS.Length; i++)
                {
                    string rmGrvKey = RMT_GRVS[i].ToString();
                    string rmQty = RMT_QTYS[i].ToString();
                    string rmGrv = "";
                    DateTime grvExpiry = DateTime.MinValue;
                    DataTable dtGrvDetails = dbHlpr.FetchData("SELECT RCE_CPGRV, RCE_EXPDT, RCV_REFNO "
                        + " FROM INV_GRV_EXPIRY "
                        + " LEFT JOIN INV_GRV_RCVNG ON RCE_GRKEY = RCV_KEY "
                        + " WHERE RCE_KEY = '" + rmGrvKey + "' ");
                    if (dtGrvDetails.Rows.Count > 0)
                    {
                        rmGrv = dtGrvDetails.Rows[0]["RCE_CPGRV"].ToString();
                        try
                        {
                            grvExpiry = Convert.ToDateTime(dtGrvDetails.Rows[0]["RCE_EXPDT"].ToString());
                        }
                        catch { }
                    }

                    string rmLinQry = "INSERT INTO PRC_TRX_YLDLN ( "
                        + " PRL_ID, PRL_BCHNO, "
                        + " PRL_SDATE, PRL_STIME, PRL_EDATE, PRL_ETIME, "
                        + " PRL_WEEK, PRL_RMCOD, PRL_WTKGS, "
                        + " PRL_ITMCD, PRL_DESC1, PRL_TYPE, PRL_GRVNO, PRL_EXPDT, PRL_QTY, "
                        + " PRL_ENTBY, PRL_ENTDT, PRL_ENTIM "
                        + " ) VALUES ( "
                        + " '" + lblProcessId + "', '" + lblProcessId.Substring(lblProcessId.IndexOf('-') + 1) + "', "
                        + " '" + StartDate + "', '" + StartTime + "', '" + EndDate + "', '" + EndTime + "', "
                        + " '" + weekToPut + "', '" + RmCd + "', '" + totalWeightIn + "', "
                        + " '" + RmCd + "', '" + RmDesc1 + "', 'RMT', '" + rmGrv + "', '" + grvExpiry + "', '" + rmQty + "', "
                        + " '" + Session["UserId"] + "', '" + dtNow + "', '" + dtNow + "' "
                        + " ) ";
                    dbHlpr.ExecuteNonQuery(rmLinQry);
                }

                // Usable Meat
                for (int i = 0; i < UMT_ITMS.Length; i++)
                {
                    string itmCode = UMT_ITMS[i].ToString();
                    string itmDesc = RawMaterial.GetByCode(UMT_ITMS[i]).Rows[0]["RMT_DESC1"].ToString();
                    string itmGrv = lblProcessId;
                    string itmQty = UMT_QTYS[i].ToString();

                    string umLinQry = "INSERT INTO PRC_TRX_YLDLN ("
                        + " PRL_ID, PRL_BCHNO, "
                        + " PRL_SDATE, PRL_STIME, PRL_EDATE, PRL_ETIME, "
                        + " PRL_WEEK, PRL_RMCOD, PRL_WTKGS, "
                        + " PRL_ITMCD, PRL_DESC1, PRL_TYPE, PRL_GRVNO, PRL_QTY, "
                        + " PRL_ENTBY, PRL_ENTDT, PRL_ENTIM "
                        + " ) VALUES ( "
                        + " '" + lblProcessId + "', '" + lblProcessId.Substring(lblProcessId.IndexOf('-') + 1) + "', "
                        + " '" + StartDate + "', '" + StartTime + "', '" + EndDate + "', '" + EndTime + "', "
                        + " '" + weekToPut + "', '" + RmCd + "', '" + totalWeightIn + "', "
                        + " '" + itmCode + "', '" + itmDesc + "', 'UMT', '" + lblProcessId + "', '" + itmQty + "', "
                        + " '" + Session["UserId"] + "', '" + dtNow + "', '" + dtNow + "' "
                        + " ) ";
                    dbHlpr.ExecuteNonQuery(umLinQry);
                }

                // Wastages
                for (int i = 0; i < WST_ITMS.Length; i++)
                {
                    string itmCode = WST_ITMS[i].ToString();
                    string itmDesc = RawMaterial.GetByCode(WST_ITMS[i]).Rows[0]["RMT_DESC1"].ToString();
                    string itmGrv = lblProcessId;
                    string itmQty = WST_QTYS[i].ToString();

                    string wsLinQry = "INSERT INTO PRC_TRX_YLDLN ("
                        + " PRL_ID, PRL_BCHNO, "
                        + " PRL_SDATE, PRL_STIME, PRL_EDATE, PRL_ETIME, "
                        + " PRL_WEEK, PRL_RMCOD, PRL_WTKGS, "
                        + " PRL_ITMCD, PRL_DESC1, PRL_TYPE, PRL_GRVNO, PRL_QTY, "
                        + " PRL_ENTBY, PRL_ENTDT, PRL_ENTIM "
                        + " ) VALUES ( "
                        + " '" + lblProcessId + "', '" + lblProcessId.Substring(lblProcessId.IndexOf('-') + 1) + "', "
                        + " '" + StartDate + "', '" + StartTime + "', '" + EndDate + "', '" + EndTime + "', "
                        + " '" + weekToPut + "', '" + RmCd + "', '" + totalWeightIn + "', "
                        + " '" + itmCode + "', '" + itmDesc + "', 'WST', '" + lblProcessId + "', '" + itmQty + "', "
                        + " '" + Session["UserId"] + "', '" + dtNow + "', '" + dtNow + "' "
                        + " ) ";
                    dbHlpr.ExecuteNonQuery(wsLinQry);
                }

                SetSuccessMessage("Process Saved Successfully");
                return Json(new { success = true, url = Url.Action("ProcessInquiry", "Yielding") });
            }

            return Json(new { success = false, msg = validateData.Value });
        }

        [NonAction]
        private KeyValuePair<bool, string> ValidateProcessData(FormCollection f)
        {
            string msg = "";
            if (f["rmt_cd"] == null || f["rmt_cd"].ToString().Length <= 0)
            {
                msg = "Raw Material is required";
                return new KeyValuePair<bool, string>(false, msg);
            }

            DateTime startdate;
            if (f["bch_start_dt"] == null || !DateTime.TryParse(f["bch_start_dt"].ToString(), out startdate))
            {
                msg = "Invalid Start Date";
                return new KeyValuePair<bool, string>(false, msg);
            }
            DateTime enddate;
            if (f["bch_end_dt"] == null || !DateTime.TryParse(f["bch_end_dt"].ToString(), out enddate))
            {
                msg = "Invalid End Date";
                return new KeyValuePair<bool, string>(false, msg);
            }

            if (startdate > enddate)
            {
                msg = "End date cannot be before Start Date";
                return new KeyValuePair<bool, string>(false, msg);
            }

            if (f["prcs_id"] == "-" || f["prcs_id"].Trim().Length < 5)
            {
                msg = "Invalid Process ID";
                return new KeyValuePair<bool, string>(false, msg);
            }

            string[] RAWMEAT_GRVS = f["rm_grvs[]"] == null ? new string[] { } : f["rm_grvs[]"].Split(',');
            string[] RAWMEAT_QTYS = f["rm_qtys[]"] == null ? new string[] { } : f["rm_qtys[]"].Split(',');
            if (RAWMEAT_GRVS.Length <= 0 || RAWMEAT_QTYS.Length <= 0 || RAWMEAT_GRVS.Length != RAWMEAT_QTYS.Length)
            {
                msg = "Raw Meat details are required. Please enter GRV and Quantity";
                return new KeyValuePair<bool, string>(false, msg);
            }

            if (f["posting"] == "true")
            {
                string rmGridErrors = "";
                for (int i = 0; i < RAWMEAT_GRVS.Length; i++)
                {
                    if (RAWMEAT_GRVS[i].ToString().Length <= 0)
                    {
                        rmGridErrors += "Item GRV # is required for all rows.<br/>";
                    }
                    if (RAWMEAT_QTYS[i].ToString().Length <= 0)
                    {
                        rmGridErrors += "Item Quantity is required for all rows<br />";
                    }
                }
                if (rmGridErrors.Trim().Length > 0)
                {
                    msg = rmGridErrors;
                    return new KeyValuePair<bool, string>(false, msg);
                }

                string[] UMT_ITMS = f["us_itmcds[]"].Split(',');
                string[] UMT_QTYS = f["us_qtys[]"].Split(',');
                string umGridErrors = "";
                for (int i = 0; i < UMT_ITMS.Length; i++)
                {
                    if (UMT_ITMS[i].ToString().Length <= 0)
                    {
                        umGridErrors += "Item Code is required for all rows<br />";
                    }
                    if (UMT_QTYS[i].ToString().Length <= 0)
                    {
                        umGridErrors += "Item Quantity is required for all rows<br />";
                    }
                }
                if (umGridErrors.Trim().Length > 0)
                {
                    msg = umGridErrors;
                    return new KeyValuePair<bool, string>(false, msg);
                }

                string[] WST_ITMS = f["ws_itmcds[]"].Split(',');
                string[] WST_QTYS = f["ws_qtys[]"].Split(',');
                string wsGridErrors = "";
                for (int i = 0; i < WST_ITMS.Length; i++)
                {
                    if (WST_QTYS[i].ToString().Length <= 0)
                    {
                        wsGridErrors += "Item Quantity is required for all rows<br />";
                    }
                }
                if (wsGridErrors.Trim().Length > 0)
                {
                    msg = umGridErrors;
                    return new KeyValuePair<bool, string>(false, msg);
                }
            }

            return new KeyValuePair<bool, string>(true, "");
        }

        [HttpPost]
        public ActionResult SaveBoM(FormCollection form)
        {
            if (form["rmt_cd"] == null || form["rmt_cd"].ToString().Trim().Length <= 0)
            {
                return Json(new
                {
                    success = false,
                    msg = "Error :: Please select a Raw Meat item to save."
                });
            }

            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            DateTime dtNow = DateTime.Now;
            string RmtCode = form["rmt_cd"].ToString().Trim();
            dbHlpr.ExecuteNonQuery("DELETE FROM BOM_TRX_YLDNG WHERE YBM_RMCOD = '" + RmtCode + "' ");
            string[] DT_USABLES = form["usable_items[]"].Split(',');
            string[] DT_WASTAGES = form["wastage_items[]"].Split(',');
            if (DT_USABLES.Length > 0 || DT_WASTAGES.Length > 0)
            {
                DataTable dt = RawMaterial.GetByCode(RmtCode);
                if (dt.Rows.Count <= 0)
                {
                    return Json(new
                    {
                        success = false,
                        msg = "Error :: Raw Material details not found."
                    });
                }

                string rmtDesc1 = dt.Rows[0]["RMT_DESC1"].ToString();
                // Insert a row for Raw Material
                dbHlpr.ExecuteNonQuery("INSERT INTO BOM_TRX_YLDNG ( "
                    + " YBM_KEY, YBM_RMCOD, YBM_DESC1, "
                    + " YBM_ITMNO, YBM_ITFLG, YBM_SEQNO, "
                    + " YBM_ENTBY, YBM_ENTDT, YBM_ENTIM, "
                    + " YBM_UPDBY, YBM_LSUPD, YBM_LSUPT "
                    + " ) VALUES ( "
                    + " '" + RmtCode + RmtCode + "', '" + RmtCode + "', '" + rmtDesc1 + "', "
                    + " '" + RmtCode + "', '1', '1', "
                    + " '" + Session["UserId"] + "', '" + dtNow + "', '" + dtNow + "', "
                    + " '" + Session["UserId"] + "', '" + dtNow + "', '" + dtNow + "' "
                    + " ) ");

                // Insert Rows for Usabale Meat
                for (int i = 0; i < DT_USABLES.Length; i++)
                {
                    string itemNo = DT_USABLES[i].ToString().Trim();
                    string ItemTypeFlag = "2";
                    int SeqNo = i + 1;

                    dbHlpr.ExecuteNonQuery("INSERT INTO BOM_TRX_YLDNG ( "
                        + " YBM_KEY, YBM_RMCOD, YBM_DESC1, "
                        + " YBM_ITMNO, YBM_ITFLG, YBM_SEQNO, "
                        + " YBM_ENTBY, YBM_ENTDT, YBM_ENTIM, "
                        + " YBM_UPDBY, YBM_LSUPD, YBM_LSUPT "
                        + " ) VALUES ( "
                        + " '" + RmtCode + itemNo + "', '" + RmtCode + "', '" + rmtDesc1 + "', "
                        + " '" + itemNo + "', '" + ItemTypeFlag + "', '" + SeqNo + "', "
                        + " '" + Session["UserId"] + "', '" + dtNow + "', '" + dtNow + "', "
                        + " '" + Session["UserId"] + "', '" + dtNow + "', '" + dtNow + "' "
                        + " ) ");
                }

                // Insert Rows for Wastages
                for (int i = 0; i < DT_WASTAGES.Length; i++)
                {
                    string itemNo = DT_WASTAGES[i].ToString().Trim();
                    string ItemTypeFlag = "3";
                    int SeqNo = DT_USABLES.Length + i + 1;

                    dbHlpr.ExecuteNonQuery("INSERT INTO BOM_TRX_YLDNG ( "
                        + " YBM_KEY, YBM_RMCOD, YBM_DESC1, "
                        + " YBM_ITMNO, YBM_ITFLG, YBM_SEQNO, "
                        + " YBM_ENTBY, YBM_ENTDT, YBM_ENTIM, "
                        + " YBM_UPDBY, YBM_LSUPD, YBM_LSUPT "
                        + " ) VALUES ( "
                        + " '" + RmtCode + itemNo + "', '" + RmtCode + "', '" + rmtDesc1 + "', "
                        + " '" + itemNo + "', '" + ItemTypeFlag + "', '" + SeqNo + "', "
                        + " '" + Session["UserId"] + "', '" + dtNow + "', '" + dtNow + "', "
                        + " '" + Session["UserId"] + "', '" + dtNow + "', '" + dtNow + "' "
                        + " ) ");
                }

                SetSuccessMessage("BoM Saved Successfully");

                return Json(new { success = true, url = Url.Action("BoM") });
            }
            else
            {
                return Json(new
                {
                    success = false,
                    msg = "Error :: No items added, unable to save."
                });
            }
        }

        [HttpPost]
        public ActionResult PostBoM(FormCollection form)
        {
            if (form["rmt_cd"] == null || form["rmt_cd"].ToString().Trim().Equals(""))
            {
                return Json(new
                {
                    success = false,
                    msg = "Error :: No BoM loaded to Post"
                });
            }

            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            DateTime dateTimeNow = DateTime.Now;
            string dateTimeNowDate = dateTimeNow.ToString("yyyy-MM-dd");
            string dateTimeNowTime = dateTimeNow.ToString("HH:mm:ss.fff");
            string RmCode = form["rmt_cd"].ToString().Trim();

            dbHlpr.ExecuteScalarQuery("INSERT INTO BOM_MST_YLDNG ( "
                + " YBM_KEY, YBM_RMCOD, YBM_DESC1, "
                + " YBM_ITMNO, YBM_ITFLG, YBM_SEQNO, "
                + " YBM_ENTBY, YBM_ENTDT, YBM_ENTIM, "
                + " YBM_UPDBY, YBM_LSUPD, YBM_LSUPT, "
                + " YBM_PSTBY, YBM_PSTDT, YBM_PSTIM "
                + " ) SELECT "
                + " YBM_KEY, YBM_RMCOD, YBM_DESC1, "
                + " YBM_ITMNO, YBM_ITFLG, YBM_SEQNO, "
                + " YBM_ENTBY, YBM_ENTDT, YBM_ENTIM, "
                + " YBM_UPDBY, YBM_LSUPD, YBM_LSUPT, "
                + " '" + Session["UserId"] + "', '" + dateTimeNowDate + "', '" + dateTimeNowTime + "' "
                + " FROM BOM_TRX_YLDNG "
                + " WHERE BOM_TRX_YLDNG.YBM_RMCOD = '" + RmCode + "'");

            dbHlpr.ExecuteScalarQuery("DELETE FROM BOM_TRX_YLDNG WHERE YBM_RMCOD = '" + RmCode + "'");

            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            string comments = "Posted MCPU Yielding BoM for "
                + RmCode + " into "
                + controllerName + "/" + actionName + " form at " + System.DateTime.Now.ToString();
            string extras = "YBM of RM Code " + RmCode + " Posted";
            dbHlpr.InsertUserLog(Session["UserId"].ToString(), Session["UserName"].ToString(), DateTime.Now, comments, extras);

            SetSuccessMessage("Yielding BoM Posted Successfully.");

            return Json(new { success = true, url = Url.Action("BoM") });
        }

        [HttpPost]
        public ActionResult GetGrvDetails(string rmtCode, string grvkey)
        {
            DataTable dtRmtDetails = RawMaterial.GetByCode(rmtCode);
            if (dtRmtDetails.Rows.Count > 0)
            {
                DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
                DataTable dtGrvDetails = dbHlpr.FetchData("SELECT RCE_EXPDT, RCV_REFNO "
                    + " FROM INV_GRV_EXPIRY "
                    + " LEFT JOIN INV_GRV_RCVNG ON RCE_GRKEY = RCV_KEY "
                    + " WHERE RCV_RMCOD = '" + dtRmtDetails.Rows[0]["RMT_LNKCD"].ToString() + "' AND RCE_KEY = '" + grvkey + "' ");

                if (dtGrvDetails.Rows.Count > 0)
                {
                    string expiryDate = DateTime.MinValue.ToString("dd-MMM-yyyy");
                    try
                    {
                        expiryDate = Convert.ToDateTime(dtGrvDetails.Rows[0]["RCE_EXPDT"]).ToString("dd-MMM-yyyy");
                    }
                    catch { }
                    return Json(new { success = true, data = new { expiry = expiryDate, refno = dtGrvDetails.Rows[0]["RCV_REFNO"].ToString() } });
                }
            }

            return Json(new { success = false, data = new { expiry = "", refno = "" } });
        }

        [HttpPost]
        public ActionResult GetOpenProcessesByRmt(string code)
        {
            DataTable dt = RawMaterial.GetByCode(code);

            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            DataTable dtData = dbHlpr.FetchData("SELECT "
                        + " PRH_ID, PRH_BCHNO, PRH_RMCOD, PRH_DESC1, "
                        + " PRH_SDATE, PRH_STIME, PRH_EDATE, PRH_ETIME, "
                        + " PRH_WEEK, PRH_WTKGS, PRH_REFNO, PRH_ENTBY, PRH_ENTDT, PRH_ENTIM, "
                        + " PRH_UPDBY, PRH_LSUPD, PRH_LSUPT "
                        + " FROM PRC_TRX_YLDHD "
                        + " WHERE PRH_RMCOD = '" + code + "' ");

            return Json(new { success = true, data = JsonConvert.SerializeObject(dt), processes = JsonConvert.SerializeObject(dtData) });
        }

        [HttpPost]
        public ActionResult LoadItems(string catg)
        {
            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            string qry = "SELECT "
                + " RMT_CODE, RMT_DESC1, RMT_DESC2, RMT_TRKMD, RMT_CATG, RMT_SCATG, "
                + " RMT_UDF1, RMT_UDF2, RMT_UDF3, RMT_UDF4, RMT_UOM, RMT_WT, "
                + " RMT_AVCST, RMT_ALCOD, RMT_STATS, RMT_LNKCD "
                + " FROM INV_MSTR_RWMAT "
                + " ORDER BY RMT_CODE ";
            if (catg == "WASTG")
            {
                qry = "SELECT "
                + " RMT_CODE, RMT_DESC1, RMT_DESC2, RMT_TRKMD, RMT_CATG, RMT_SCATG, "
                + " RMT_UDF1, RMT_UDF2, RMT_UDF3, RMT_UDF4, RMT_UOM, RMT_WT, "
                + " RMT_AVCST, RMT_ALCOD, RMT_STATS, RMT_LNKCD "
                + " FROM INV_MSTR_RWMAT "
                + " WHERE RMT_CATG = '" + catg + "' "
                + " ORDER BY RMT_CODE ";
            }
            DataTable dt = dbHlpr.FetchData(qry);

            return Json(new { success = true, data = JsonConvert.SerializeObject(dt) });
        }

        [HttpPost]
        public ActionResult GetBomByRmtCode(string code)
        {
            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
            DataTable YldBoMHistDetails = dbHlpr.FetchData("SELECT "
                        + " YBM_KEY, YBM_RMCOD, YBM_DESC1, "
                        + " YBM_ITMNO, YBM_ITFLG, YBM_SEQNO, "
                        + " YBM_ENTBY, YBM_ENTDT, YBM_ENTIM, "
                        + " YBM_UPDBY, YBM_LSUPD, YBM_LSUPT, "
                        + " YBM_PSTBY, YBM_PSTDT, YBM_PSTIM "
                        + " FROM BOM_MST_YLDNG "
                        + " WHERE YBM_RMCOD = '" + code + "' ");
            if (YldBoMHistDetails.Rows.Count > 0)
            {
                return Json(new
                {
                    success = false,
                    msg = "Attention: BoM already exist and posted."
                });
            }

            DataTable DT_USABLES = new DataTable();
            DataTable DT_WASTAGES = new DataTable();
            DataTable YldBoMTrxDetails = dbHlpr.FetchData("SELECT "
                + " YBM_KEY, YBM_RMCOD, YBM_DESC1, "
                + " YBM_ITMNO, YBM_ITFLG, YBM_SEQNO, "
                + " YBM_ENTBY, YBM_ENTDT, YBM_ENTIM, "
                + " YBM_UPDBY, YBM_LSUPD, YBM_LSUPT "
                + " FROM BOM_TRX_YLDNG "
                + " WHERE YBM_RMCOD = '" + code + "' ");
            if (YldBoMTrxDetails.Rows.Count > 0)
            {
                DT_USABLES = dbHlpr.FetchData("SELECT "
                    + " YBM_ITMNO AS ITMCOD, RMT_DESC1 AS ITMDESC1, RMT_UOM AS ITMUOM "
                    + " FROM BOM_TRX_YLDNG "
                    + " LEFT JOIN INV_MSTR_RWMAT ON YBM_ITMNO = RMT_CODE "
                    + " WHERE YBM_ITFLG = '2' "
                    + " AND YBM_RMCOD = '" + code + "' "
                    + " ORDER BY YBM_SEQNO ");
                DT_WASTAGES = dbHlpr.FetchData("SELECT "
                    + " YBM_ITMNO AS ITMCOD, RMT_DESC1 AS ITMDESC1, RMT_UOM AS ITMUOM "
                    + " FROM BOM_TRX_YLDNG "
                    + " LEFT JOIN INV_MSTR_RWMAT ON YBM_ITMNO = RMT_CODE "
                    + " WHERE YBM_ITFLG = '3' "
                    + " AND YBM_RMCOD = '" + code + "' "
                    + " ORDER BY YBM_SEQNO ");

            }

            return Json(new
            {
                success = true,
                usableData = JsonConvert.SerializeObject(DT_USABLES),
                wastgData = JsonConvert.SerializeObject(DT_WASTAGES)
            });
        }

    }
}