﻿using MCPU.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MCPU.Controllers.Admin.Setup
{
    public class UsersController : AuthController
    {
        string INDEX_VIEW = "~/Views/Admin/System/Users/Index.cshtml";
        string EDIT_VIEW = "~/Views/Admin/System/Users/Edit.cshtml";
        DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
        //
        // GET: /Setup/
        public ActionResult Index()
        {
            List<UserModel> model = UserModel.GetAll();

            ViewBag.POST_DATA = new FormCollection();
            return View(INDEX_VIEW, model);
        }

        [HttpPost]
        public ActionResult Delete(FormCollection form)
        {
            string usrCode = form["usr_code"];
            dbHlpr.ExecuteNonQuery("DELETE FROM STP_MSTR_USERS WHERE USR_ID = '" + usrCode + "'");

            SetSuccessMessage("User Deleted Successfully");

            return RedirectToAction("Index");
        }

        public ActionResult Create()
        {
            ViewBag.AllowCodeEdit = true;
            UserModel model = new UserModel();

            LoadFormData();

            return PartialView(EDIT_VIEW, model);
        }

        public ActionResult Edit(string code)
        {
            ViewBag.AllowCodeEdit = false;

            UserModel model = UserModel.GetByCode(code);

            LoadFormData();

            return PartialView(EDIT_VIEW, model);
        }

        [HttpPost]
        public ActionResult Edit(UserModel usrData)
        {
            if (!TryValidateModel(usrData))
            {
                ViewBag.AllowCodeEdit = usrData.UserId == null || usrData.UserId.Length <= 0;
                LoadFormData();
                return PartialView(EDIT_VIEW, usrData);
            }

            DataTable dtUserOfEmpNo = dbHlpr.FetchData("SELECT * FROM STP_MSTR_USERS WHERE USR_EMPID = '" + usrData.EmpId + "' AND USR_ID <> '" + usrData.UserId + "' ");
            if (dtUserOfEmpNo.Rows.Count > 0)
            {
                ModelState.AddModelError("EmpId", "Employee Code already exists.");

                ViewBag.AllowCodeEdit = usrData.UserId == null || usrData.UserId.Length <= 0;
                LoadFormData();
                return PartialView(EDIT_VIEW, usrData);
            }
            else
            {
                UserModel myDT = UserModel.GetByCode(usrData.UserId);
                if (myDT == null)
                {
                    dbHlpr.ExecuteNonQuery("INSERT INTO STP_MSTR_USERS ( "
                        + " USR_ID, USR_INITL, USR_NAME, USR_PSWRD, USR_EMPID, USR_DEPT,  "
                        + " USR_MNUCD, USR_SYACS, USR_DTGOV "
                        + " ) VALUES ( "
                        + " '" + usrData.UserId + "', '" + usrData.Initials + "', '" + usrData.Name + "', "
                        + " '" + usrData.Password + "', '" + usrData.EmpId + "', '" + usrData.Department + "', "
                        + " '" + usrData.MenuCode + "', '" + usrData.SystemAccess + "', "
                        + " '" + usrData.EditMasters + "')");

                    SetSuccessMessage("User Created Successfully");
                }
                else
                {
                    dbHlpr.ExecuteNonQuery("UPDATE [STP_MSTR_USERS] SET "
                        + " USR_INITL = '" + usrData.Initials + "', "
                        + " USR_NAME = '" + usrData.Name + "', "
                        //+ " USR_PSWRD = '" + userPassword + "', "
                        + " USR_EMPID = '" + usrData.EmpId + "', "
                        + " USR_DEPT = '" + usrData.Department + "', "
                        + " USR_MNUCD = '" + usrData.MenuCode + "', "
                        + " USR_SYACS = '" + usrData.SystemAccess + "', "
                        + " USR_DTGOV = '" + usrData.EditMasters + "' "
                        + " WHERE USR_ID = '" + usrData.UserId + "' ");

                    SetSuccessMessage("User Updated Successfully");
                }
            }

            return Json(new { url = Url.Action("Index") });
        }

        [NonAction]
        private void LoadFormData()
        {
            List<DepartmentModel> deptsList = DepartmentModel.GetAll();
            ViewBag.Departments = deptsList;
            
            List<MenuModel> menusList = MenuModel.GetAll();
            ViewBag.Menus = menusList;            
        }

    }
}