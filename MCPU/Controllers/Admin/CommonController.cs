﻿using MCPU.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MCPU.Controllers.Admin
{
    public class CommonController : AuthController
    {
        [HttpPost]
        public ActionResult GetRmtByCode(string code)
        {
            DataTable dt = RawMaterial.GetByCode(code);

            return Json(new { success = true, data = JsonConvert.SerializeObject(dt) });
        }

        [HttpPost]
        public ActionResult GetProductByCode(string code)
        {
            DataTable dt = Product.GetByCode(code);

            return Json(new { success = true, data = JsonConvert.SerializeObject(dt) });
        }
    }
}