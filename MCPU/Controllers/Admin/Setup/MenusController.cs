﻿using MCPU.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MCPU.Controllers.Admin.Setup
{
    public class MenusController : AuthController
    {
        string INDEX_VIEW = "~/Views/Admin/Setup/Menus/Index.cshtml";
        string EDIT_VIEW = "~/Views/Admin/Setup/Menus/Edit.cshtml";
        DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
        //
        // GET: /Setup/
        public ActionResult Index()
        {
            List<MenuModel> model = MenuModel.GetAll();

            ViewBag.POST_DATA = new FormCollection();
            return View(INDEX_VIEW, model);
        }

        [HttpPost]
        public ActionResult Delete(FormCollection form)
        {
            string menuCode = form["menu_code"];
            MenuModel.DeleteMenu(menuCode);

            SetSuccessMessage("Menu Deleted Successfully");

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult MenuCodeSelectionChanged(string code)
        {
            MenuModel menuDetails = MenuModel.GetByCode(code);

            List<EasyUiTreeNode> treeData = TreeData(code);

            return Json(new { success = true, data = menuDetails, TreeInfo = treeData });
        }


        public List<EasyUiTreeNode> TreeData(string menuCode)
        {
            List<EasyUiTreeNode> pages = MenuItemModel.GetPagesTree(0);
            if (menuCode.Length == 0)
            {
                return PagesTreeJson(pages, new List<int>());
            }
            else
            {
                List<MenuItemModel> menuInfo = MenuModel.GetPagesByCode(menuCode);
                List<int> pageIds = new List<int>();
                foreach (MenuItemModel mnuInf in menuInfo)
                {
                    pageIds.Add(mnuInf.Id);
                }
                return PagesTreeJson(pages, pageIds);
            }
        }

        private List<EasyUiTreeNode> PagesTreeJson(List<EasyUiTreeNode> pages, List<int> checkedIds)
        {
            List<EasyUiTreeNode> arr = new List<EasyUiTreeNode>();
            foreach (EasyUiTreeNode pg in pages)
            {
                EasyUiTreeNode temp = new EasyUiTreeNode();
                temp.id = pg.id;
                temp.text = "<span style='display: none;'>" + pg.id + "</span>" + pg.text;
                temp.@checked = false;
                if (pg.children.Count > 0)
                {
                    temp.children = PagesTreeJson(pg.children, checkedIds);
                }
                else
                {
                    temp.@checked = checkedIds.Contains(pg.id) ? true : false;
                }
                arr.Add(temp);
            }
            return arr;
        }

        [HttpPost]
        public ActionResult Save(FormCollection form)
        {
            bool validRequest = true;
            if (form["menu_code"].Trim().Length == 0)
            {
                SetFailMessage("Failure:: Menu save failed, Menu Code is required.");
                validRequest = false;
            }
            if (form["menu_name"].Trim().Length == 0)
            {
                SetFailMessage("Failure:: Menu save failed, Menu Name is required.");
                validRequest = false;
            }
            if (form["menu_pages"].Trim().Length == 0)
            {
                SetFailMessage("Failure:: Menu save failed, Menu Pages is required.");
                validRequest = false;
            }

            if (validRequest)
            {
                string menuCode = form["menu_code"];
                string menuName = form["menu_name"];
                string menuPages = form["menu_pages"];

                MenuModel myDT = MenuModel.GetByCode(menuCode);
                if (myDT == null)
                {
                    dbHlpr.ExecuteNonQuery("INSERT INTO STP_USR_MENUS (MNU_CODE, MNU_NAME) "
                    + " VALUES ('" + menuCode + "', '" + menuName + "')");

                    SaveMenuPages(menuCode, menuName, menuPages);

                    SetSuccessMessage("Menu Created Successfully");
                }
                else
                {
                    dbHlpr.ExecuteNonQuery("UPDATE STP_USR_MENUS SET "
                    + " MNU_CODE = '" + menuCode + "', "
                    + " MNU_NAME = '" + menuName + "' "
                    + " WHERE MNU_CODE = '" + menuCode + "'");

                    SaveMenuPages(menuCode, menuName, menuPages);

                    SetSuccessMessage("Menu Updated Successfully");
                }
            }

            return RedirectPermanent(Url.Action("Index"));
        }

        [NonAction]
        private void SaveMenuPages(string code, string name, string pages)
        {
            DataTable mnuData = new DataTable();
            mnuData.Columns.Add("mnuCode");
            mnuData.Columns.Add("mnuName");
            mnuData.Columns.Add("mnuPageId");

            string[] pageIds = pages.Split(',');
            foreach (string pgId in pageIds)
            {
                DataRow mnuEntry = mnuData.NewRow();

                mnuEntry["mnuCode"] = code;
                mnuEntry["mnuName"] = name;
                mnuEntry["mnuPageId"] = pgId;

                mnuData.Rows.Add(mnuEntry);
            }

            MenuModel.AddNewMenu(mnuData);
        }
    }
}