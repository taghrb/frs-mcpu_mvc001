﻿using MCPU.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MCPU.Controllers.Admin.Setup
{
    public class MenuItemsController : AuthController
    {
        string INDEX_VIEW = "~/Views/Admin/Setup/MenuItems/Index.cshtml";
        string EDIT_VIEW = "~/Views/Admin/Setup/MenuItems/Edit.cshtml";
        DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
        //
        // GET: /Setup/
        public ActionResult Index()
        {
            DataTable dt = dbHlpr.FetchData("SELECT * FROM STP_MNU_PAGES");

            List<MenuItemModel> model = new List<MenuItemModel>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                model.Add(new MenuItemModel(
                    (int)dt.Rows[i]["PAG_ID"],
                    dt.Rows[i]["PAG_CODE"].ToString(),
                    dt.Rows[i]["PAG_NAME"].ToString(),
                    dt.Rows[i]["PAG_URL"].ToString(),
                    dt.Rows[i]["PAG_TYPE"].ToString(),
                    (int)dt.Rows[i]["PAG_PARNT"],
                    (int)dt.Rows[i]["PAG_SEQNO"]
                    ));
            }

            ViewBag.POST_DATA = new FormCollection();
            return View(INDEX_VIEW, model);
        }

        [HttpPost]
        public ActionResult Delete(FormCollection form)
        {
            string mnuCode = form["mnupg_code"];
            dbHlpr.ExecuteNonQuery("DELETE FROM STP_MNU_PAGES WHERE PAG_CODE = '" + mnuCode + "'");

            SetSuccessMessage("Menu Page Deleted Successfully");

            return RedirectToAction("Index");
        }

        public ActionResult Create()
        {
            ViewBag.AllowCodeEdit = true;
            MenuItemModel model = new MenuItemModel();

            LoadParentPages();

            return PartialView(EDIT_VIEW, model);
        }

        public ActionResult Edit(string code)
        {
            ViewBag.AllowCodeEdit = false;

            MenuItemModel model = MenuItemModel.GetByCode(code);

            LoadParentPages();

            return PartialView(EDIT_VIEW, model);
        }

        [HttpPost]
        public ActionResult Edit(MenuItemModel mnuData)
        {
            if (!TryValidateModel(mnuData))
            {
                ViewBag.AllowCodeEdit = mnuData.Code == null || mnuData.Code.Length <= 0;
                LoadParentPages();
                return PartialView(EDIT_VIEW, mnuData);
            }

            if (mnuData.Type.Equals("T") || mnuData.Type.ToString().Substring(0, 1).Equals("T"))
            {
                mnuData.Parent = 0;
            }
            else if (mnuData.Parent == null || mnuData.Parent <= 0)
            {
                ModelState.AddModelError("Parent", "Parent field is required");

                ViewBag.AllowCodeEdit = mnuData.Code == null || mnuData.Code.Length <= 0;
                LoadParentPages();
                return PartialView(EDIT_VIEW, mnuData);
            }

            DataTable myDT = dbHlpr.FetchData("SELECT * FROM STP_MNU_PAGES WHERE PAG_CODE = '" + mnuData.Code + "'");
            if (myDT.Rows.Count <= 0)
            {
                dbHlpr.ExecuteNonQuery("INSERT INTO STP_MNU_PAGES (PAG_CODE, PAG_SEQNO, PAG_NAME, PAG_PARNT, PAG_TYPE, PAG_URL) "
                    + " VALUES ( "
                    + " '" + mnuData.Code + "', "
                    + " '" + mnuData.SeqNo + "', "
                    + " '" + mnuData.Name + "', "
                    + " '" + mnuData.Parent + "', "
                    + " '" + ((char)mnuData.Type).ToString() + "', "
                    + " '" + mnuData.Url + "' "
                    + " )");

                SetSuccessMessage("Menu Page Created Successfully");
            }
            else
            {
                dbHlpr.ExecuteNonQuery("UPDATE STP_MNU_PAGES SET "
                    + " PAG_TYPE = '" + ((char)mnuData.Type).ToString() + "', "
                    + " PAG_SEQNO = '" + mnuData.SeqNo + "', "
                    + " PAG_NAME = '" + mnuData.Name + "', "
                    + " PAG_URL = '" + mnuData.Url + "', "
                    + " PAG_PARNT = '" + mnuData.Parent + "' "
                    + " WHERE PAG_CODE = '" + mnuData.Code + "'");

                SetSuccessMessage("Menu Page Updated Successfully");
            }

            return Json(new { url = Url.Action("Index") });
        }

        [NonAction]
        private void LoadParentPages()
        {
            DataTable dt = dbHlpr.FetchData("SELECT * FROM STP_MNU_PAGES");

            List<MenuItemModel> parentsList = new List<MenuItemModel>();
            // parentsList.Add(new MenuItemModel(0, "", "No Parent", "", "", 0, 0));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                parentsList.Add(new MenuItemModel(
                    (int)dt.Rows[i]["PAG_ID"],
                    dt.Rows[i]["PAG_CODE"].ToString(),
                    dt.Rows[i]["PAG_NAME"].ToString(),
                    dt.Rows[i]["PAG_URL"].ToString(),
                    dt.Rows[i]["PAG_TYPE"].ToString(),
                    (int)dt.Rows[i]["PAG_PARNT"],
                    (int)dt.Rows[i]["PAG_SEQNO"]
                    ));
            }
            ViewBag.ParentMenuPages = parentsList;
        }

    }
}