﻿using MCPU.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MCPU.Controllers.Admin.Setup
{
    public class DepartmentsController : AuthController
    {
        string INDEX_VIEW = "~/Views/Admin/Setup/Departments/Index.cshtml";
        string EDIT_VIEW = "~/Views/Admin/Setup/Departments/Edit.cshtml";
        DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
        //
        // GET: /Setup/
        public ActionResult Index()
        {
            List<DepartmentModel> model = DepartmentModel.GetAll();

            ViewBag.POST_DATA = new FormCollection();
            return View(INDEX_VIEW, model);
        }

        [HttpPost]
        public ActionResult Delete(FormCollection form)
        {
            string deptCode = form["department_code"];
            dbHlpr.ExecuteNonQuery("DELETE FROM STP_MSTR_DEPRT WHERE DPT_CODE = '" + deptCode + "'");

            SetSuccessMessage("Department Deleted Successfully");

            return RedirectToAction("Index");
        }

        public ActionResult Create()
        {
            ViewBag.AllowCodeEdit = true;
            DepartmentModel model = new DepartmentModel("", "");

            return PartialView(EDIT_VIEW, model);
        }
        public ActionResult Edit(string code)
        {
            ViewBag.AllowCodeEdit = false;

            DepartmentModel model = DepartmentModel.GetByCode(code);

            return PartialView(EDIT_VIEW, model);
        }

        [HttpPost]
        public ActionResult Edit(DepartmentModel dptData)
        {
            if (!TryValidateModel(dptData))
            {
                ViewBag.AllowCodeEdit = dptData.Code == null || dptData.Code.Length <= 0;
                return PartialView(EDIT_VIEW, dptData);
            }

            DepartmentModel myDT = DepartmentModel.GetByCode(dptData.Code);
            if (myDT == null)
            {
                dbHlpr.ExecuteNonQuery("INSERT INTO STP_MSTR_DEPRT (DPT_CODE, DPT_NAME) "
                + " VALUES ('" + dptData.Code + "', '" + dptData.Name + "')");

                SetSuccessMessage("Department Created Successfully");
            }
            else
            {
                dbHlpr.ExecuteNonQuery("UPDATE STP_MSTR_DEPRT SET "
                + " DPT_CODE = '" + dptData.Code + "', "
                + " DPT_NAME = '" + dptData.Name + "' "
                + " WHERE DPT_CODE = '" + dptData.Code + "'");

                SetSuccessMessage("Department Updated Successfully");
            }

            return Json(new { url = Url.Action("Index") });
        }

    }
}