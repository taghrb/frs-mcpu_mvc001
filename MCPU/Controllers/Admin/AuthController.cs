﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MCPU.Controllers.Admin
{
    public class AuthController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (Session["isLoggedIn"] == null || (bool)Session["isLoggedIn"] == false)
            {
                filterContext.Result = new RedirectResult("~/Login/");
            }

            DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

            var menu = new Dictionary<DataRow, Dictionary<DataRow, List<DataRow>>>();

            string userMenuCode = Session["userMenuCode"] != null ? Session["userMenuCode"].ToString() : "";
            DataTable mnuTop = dbHlpr.FetchData("SELECT STP_LNK_MNACS.*, STP_MNU_PAGES.PAG_SEQNO, STP_MNU_PAGES.PAG_URL, STP_MNU_PAGES.PAG_ICON FROM STP_LNK_MNACS JOIN STP_MNU_PAGES ON MNA_PGCOD = PAG_CODE WHERE MNA_MNCOD = '" + userMenuCode + "' AND MNA_PGTYP = 'T' ORDER BY PAG_SEQNO");
            for (int t = 0; t < mnuTop.Rows.Count; t++)
            {
                string topId = mnuTop.Rows[t]["MNA_PGID"].ToString();
                DataTable mnuGrp = dbHlpr.FetchData("SELECT STP_LNK_MNACS.*, STP_MNU_PAGES.PAG_URL, STP_MNU_PAGES.PAG_ICON FROM STP_LNK_MNACS JOIN STP_MNU_PAGES ON MNA_PGCOD = PAG_CODE WHERE PAG_PARNT = '" + topId + "' AND MNA_MNCOD = '" + userMenuCode + "' AND MNA_PGNAM != 'Merged Orders' AND MNA_PGTYP IN ('G', 'P') ORDER BY PAG_SEQNO");
                if (mnuGrp.Rows.Count > 0)
                {
                    var dictGrp = new Dictionary<DataRow, List<DataRow>>();
                    for (int g = 0; g < mnuGrp.Rows.Count; g++)
                    {
                        string grpId = mnuGrp.Rows[g]["MNA_PGID"].ToString();
                        DataTable dtPgs = dbHlpr.FetchData("SELECT STP_LNK_MNACS.*, STP_MNU_PAGES.PAG_URL, STP_MNU_PAGES.PAG_ICON FROM STP_LNK_MNACS JOIN STP_MNU_PAGES ON MNA_PGCOD = PAG_CODE WHERE PAG_PARNT = '" + grpId + "' AND MNA_MNCOD = '" + userMenuCode + "' AND MNA_PGNAM != 'Merged Orders' ORDER BY PAG_SEQNO");
                        if (dtPgs.Rows.Count > 0)
                        {
                            var listPage = new List<DataRow>();
                            for (int p = 0; p < dtPgs.Rows.Count; p++)
                            {
                                listPage.Add(dtPgs.Rows[p]);
                            }

                            dictGrp.Add(mnuGrp.Rows[g], listPage);
                        }
                        else
                        {
                            dictGrp.Add(mnuGrp.Rows[g], new List<DataRow>());
                        }
                    }
                    menu.Add(mnuTop.Rows[t], dictGrp);
                }
                else
                {
                    menu.Add(mnuTop.Rows[t], new Dictionary<DataRow, List<DataRow>>());
                }
            }


            ViewData["MenuData"] = menu;
        }

        protected void SetSuccessMessage(string msg)
        {
            Dictionary<string, string> dictMsg = new Dictionary<string, string>();
            dictMsg.Add("alert-success", msg);
            TempData["msg"] = dictMsg;
        }

        protected void SetFailMessage(string msg)
        {
            Dictionary<string, string> dictMsg = new Dictionary<string, string>();
            dictMsg.Add("alert-danger", msg);
            TempData["msg"] = dictMsg;
        }
    }
}