﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MCPU.Controllers.Admin
{
    public class HomeController : AuthController
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}